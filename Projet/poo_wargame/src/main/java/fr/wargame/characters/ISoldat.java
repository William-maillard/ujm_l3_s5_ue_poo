package fr.wargame.characters;

import fr.wargame.map.Position;

/**
 * Definis les fonctionnalitees de base d'un Soldat, et contient les Enum des
 * types des soldats
 **/
public interface ISoldat {

   public final int PORTEE_VISUELLE_MAX = 5;

   /** Les Types possybles pour les Heros **/
   static enum TypesH {
      HUMAIN(40, 3, 10, 2), NAIN(80, 1, 20, 0), ELF(70, 5, 10, 6), HOBBIT(20, 3, 5, 2);

      private final int POINTS_DE_VIE, PORTEE_VISUELLE, PUISSANCE, TIR;

      TypesH(int points, int portee, int puissance, int tir) {
         POINTS_DE_VIE = points;
         PORTEE_VISUELLE = portee;
         PUISSANCE = puissance;
         TIR = tir;
      }

      public int getPoints() {
         return POINTS_DE_VIE;
      }

      public int getPortee() {
         return PORTEE_VISUELLE;
      }

      public int getPuissance() {
         return PUISSANCE;
      }

      public int getTir() {
         return TIR;
      }

      public static TypesH getTypeHAlea() {
         return values()[(int) (Math.random() * values().length)];
      }
   }

   /** Les Types possybles pour les Monstres **/
   public static enum TypesM {
      TROLL(100, 1, 30, 0), ORC(40, 2, 10, 3), GOBELIN(20, 2, 5, 2);

      private final int POINTS_DE_VIE, PORTEE_VISUELLE, PUISSANCE, TIR;

      TypesM(int points, int portee, int puissance, int tir) {
         POINTS_DE_VIE = points;
         PORTEE_VISUELLE = portee;
         PUISSANCE = puissance;
         TIR = tir;
      }

      public int getPoints() {
         return POINTS_DE_VIE;
      }

      public int getPortee() {
         return PORTEE_VISUELLE;
      }

      public int getPuissance() {
         return PUISSANCE;
      }

      public int getTir() {
         return TIR;
      }

      public static TypesM getTypeMAlea() {
         return values()[(int) (Math.random() * values().length)];
      }
   }

   int getPoints();

   int getTour();

   int getPortee();

   void combat(Soldat soldat);

   void seDeplace(Position newPos);

   /**
    * Permet de savoir si le soldat courante est mort,
    * et de jouer un effet sonore si c'est le cas.
    * 
    * @return
    *         <ul>
    *         <li>true si le soldat est mort.</li>
    *         <li>false si le soldat est vivant</li>
    *         </ul>
    */
   boolean estMort();
}