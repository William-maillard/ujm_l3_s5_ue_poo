package fr.wargame.characters;

import java.util.LinkedList;

import fr.wargame.map.Carte;
import fr.wargame.map.Position;

/**
 * Classe permettant de definir une strategie de
 * jeu pour l'ordinateur.
 *
 */
public class GeneralMonstre {

	private Carte carte;
	private LinkedList<Monstres> troupes;

	/* ----- Attributs pour la strategie de l'ordinateur ----- */
	private int tactique;
	/** liste remplis lors du parcours en largeur de la carte. **/
	public LinkedList<Heros> herosVisibles;

	/** definis les differents etats des monstres **/
	private enum Action {
		ATTAQUE_AVANTAGE, ATTAQUE_DESAVANTAGE, ATTAQUE_DISTANCE,
		FUIR, SE_RAPPROCHE, REPOS, EXPLORE, AIDE;
	}

	/** Stocke l'etat des monstres a chaque tour **/
	private Action[] actionTroupe;

	/** Stocke la position de l'action du monstre **/
	private Position[] positionAction;

	/** directions possibles pour un monstre **/
	private enum Direction {
		N, NE, E, SE, S, SO, O, NO;

		public static Direction getDirectionAlea() {
			return values()[(int) (Math.random() * values().length)];
		}
	};

	/**
	 * Cree un GeneralMonstre avec comme infos
	 * son armee de monstre et la carte du jeu.
	 * 
	 * @param monstres : LinkedList des monstres
	 * @param carte    : Carte du jeu
	 */
	public GeneralMonstre(LinkedList<Monstres> monstres, Carte carte) {
		troupes = monstres;
		this.carte = carte;
		herosVisibles = new LinkedList<Heros>();
		int n = troupes.size();
		actionTroupe = new Action[n];
		positionAction = new Position[n];
		tactique = 0;
	}

	/**
	 * Fait jouer un tour de jeu a l'odinateur.
	 */
	public void jouerTour() {

		// Melange des strategies pour que l'ennemi ne soit pas previsible

		if (tactique < 3) {
			strategieBasique();
			tactique++;
		} else {
			strategieAmelioree();
			if (tactique > 6) {
				tactique = (int) (Math.random() * 3);
			}
		}
	}

	/**
	 * Joue un tour avec la strategie de base.
	 */
	private void strategieBasique() {
		int i = 0;

		while (i < troupes.size()) {
			jouerMonstreBasique(troupes.get(i));
			i++;
		}
	}

	/**
	 * Fonction qui impl�mente une strategie basique :
	 * - si un Heros a sa portee, alors il l'attaque.
	 * - sinon il se deplace aleatoirement.
	 * 
	 * @param m : le monstre a jouer
	 */
	private void jouerMonstreBasique(Monstres m) { // si hero autours attaque sinon se deplace aleatoirement
		Heros h = carte.trouveHeros(m.getPosition());
		if (h != null) {
			m.combat(h);
			if (h.estMort()) {
				carte.mort(h);
			} else {
				carte.mort(m);
			}
		} else {
			Position tempP = carte.trouvePositionVide(m.getPosition());
			if (tempP != null) {
				carte.deplaceSoldat(tempP, m);
				m.seDeplace(tempP);
			}
		}
	}

	/**
	 * Permet de jouer les monstres avec une meilleur strategie que celle de base
	 **/
	private void strategieAmelioree() {
		int n, i = 0;
		// boolean desavantagePuissance, desavantageTir, desavantagePv;
		Heros h;
		Monstres m;

		// On melange l'odre des troupes
		Object[] troupes = this.troupes.toArray();
		melangeTableau(troupes);

		for (i = 0; i < troupes.length; i++) {
			m = (Monstres) troupes[i];
			System.out.println("Debut decision monstre " + m);

			/*
			 * On analyse les cases a la portee du monstre
			 * et recupere les heros visibles
			 */
			carte.parcoursLargeur(m);
			System.out.println("Le monstre a " + herosVisibles.size() + " visibles");

			n = herosVisibles.size();
			if (n > 0) {

				// On choisie un heros aleatoirement :
				h = herosVisibles.get((int) (Math.random() * n));
				positionAction[i] = h.getPosition();
				// On vide la liste :
				herosVisibles.clear();

				/* Prend une decision celon les desavantages observes */

				// Peut-on attaquer au corps-a-corps ?
				if (m.getPosition().estVoisine(h.getPosition())) {
					System.out.println("attaque au corp-a-acorps possible");
					if (m.getPoints() < 2 * h.getPuissanceFrappe()) {
						// Peut-on fuir ?
						if (m.getPortee() > h.getPortee()) {
							actionTroupe[i] = Action.FUIR;
						} else {
							// on ne peut pas fuir, on attaque:
							actionTroupe[i] = Action.ATTAQUE_DESAVANTAGE;
						}
					} else { // On a assez de pv on attaque :
						actionTroupe[i] = Action.ATTAQUE_AVANTAGE;
					}
				}

				else { // distance
					System.out.println("Combat a distance posssible");
					// Peut-on attaquer a distance :
					if (carte.trajectoireTir(m.getPosition(), h.getPosition())) {
						System.out.println("Le monstre peut attaquer a distance.");
						if (h.getPortee() >= m.getPortee()) { // l'ennemi peut repliquer
							if (h.getPuissanceFrappe() > h.getTir()) { // heros + fort au corps-a-corps
								if (carte.accessible(m.getPosition()) && m.getPoints() > 2 * h.getPuissanceFrappe()) {
									actionTroupe[i] = Action.SE_RAPPROCHE;
								} else {
									actionTroupe[i] = Action.FUIR;
								}
							} else {
								actionTroupe[i] = Action.ATTAQUE_DISTANCE;
							}
						} else { // l'ennemi ne peut pas nous voir, on attaque
							actionTroupe[i] = Action.ATTAQUE_DISTANCE;
						}
					} else {
						System.out.println("Tir impossible");
						if (m.getPoints() < m.getType().getPoints() / 2) {
							actionTroupe[i] = Action.REPOS;
						} else {
							actionTroupe[i] = Action.SE_RAPPROCHE;
						}
					}
				}
			} else { // pas de Heros visible pour ce monstre.
				System.out.println("Pas de Heros en vue.");
				if (m.getPoints() <= (m.getType().getPoints() / 2)) {
					// recupere de la vie
					actionTroupe[i] = Action.REPOS;
				} else {
					actionTroupe[i] = Action.EXPLORE;
				}
			}

			System.out.println("Decision action monstre " + i + " : " + actionTroupe[i]);

			/* On a definis une action pour le monstre */
			// On verifie que les monstres precedents ne sont pas dans une mauvaise
			// situation :
			/*
			 * NB : amelioration : partir de j = i et decremente i, cela permet de compter
			 * le nb de Monstres
			 * en 'mode' Action.AIDE et permet de decider de na pas aller aider un monstre
			 * en difficulte si
			 * de l'aide est en route
			 */
			boolean stop = false;
			for (int j = 0; j < i && !stop; j++) {
				if (actionTroupe[j] == Action.ATTAQUE_DESAVANTAGE || actionTroupe[j] == Action.FUIR) {
					// peut-on se rapprocher pour l'aider ?
					if (m.getPosition().estVoisine(positionAction[j], m.getPortee() * 3)) {
						actionTroupe[i] = Action.AIDE;
						positionAction[i] = positionAction[j];
						stop = true;
					}
				}
			}
			System.out.println("action du monstre retenu : " + actionTroupe[i]);
			// On le joue :
			jouerActionMonstre(m, i);

		}
	}

	/**
	 * Joue un monstre suivant son etat contenu dans l'attrbut actionTroupe.
	 * 
	 * @param m : le monstre a jouer
	 * @param i : l'indice du monstre
	 */
	private void jouerActionMonstre(Monstres m, int i) {
		Direction d;
		Position p;

		switch (actionTroupe[i]) {
			case ATTAQUE_AVANTAGE:
			case ATTAQUE_DESAVANTAGE:
			case ATTAQUE_DISTANCE:
				p = positionAction[i];
				break;
			case FUIR:
				d = obtenirDirection(m.getPosition(), positionAction[i]);
				d = directionOpposee(d);
				p = avanceMonstre(d, m, i);
				break;
			case SE_RAPPROCHE:
			case AIDE:
				d = obtenirDirection(m.getPosition(), positionAction[i]);
				p = avanceMonstre(d, m, i);
				break;
			case REPOS:
				return; // le monstre ne joue pas
			default /* EXPLORE */:
				d = directionAlea(m);
				p = avanceMonstre(d, m, i);
				break;

		}
		if (p instanceof Position)
			carte.actionMonstre(m.getPosition(), p);
	}

	/**
	 * Cherche dans quelle direction est la position cible par rapport
	 * a la position de depart.
	 * 
	 * @param pos      : position de depart
	 * @param posCible : position cible
	 * @return Direction
	 */
	private Direction obtenirDirection(Position pos, Position posCible) {
		if (posCible.getX() == pos.getX()) {
			if (posCible.getY() < pos.getY()) {
				return Direction.N;
			} else {
				return Direction.S;
			}
		} else if (posCible.getX() < pos.getX()) {
			if (posCible.getY() == pos.getY()) {
				return Direction.O;
			} else if (posCible.getY() < pos.getY()) {
				return Direction.NO;
			} else {
				return Direction.SO;
			}
		} else {
			if (posCible.getY() == pos.getY()) {
				return Direction.E;
			} else if (posCible.getY() < pos.getY()) {
				return Direction.NE;
			} else {
				return Direction.SE;
			}
		}
	}

	/**
	 * Renvoie la direction oppose a d
	 * 
	 * @param d : une direction
	 * @return une direction
	 */
	private Direction directionOpposee(Direction d) {
		switch (d) {
			case N:
				return Direction.S;
			case NE:
				return Direction.SE;
			case E:
				return Direction.O;
			case SE:
				return Direction.NE;
			case S:
				return Direction.N;
			case SO:
				return Direction.NO;
			case O:
				return Direction.E;
			default /* NO */:
				return Direction.SO;
		}
	}

	/**
	 * Utilise pour trouve une direction aleatoire
	 * a explore pour un Monstre
	 * 
	 * @param m le monstre dont on cherche une direction
	 * @return une direction aleatoire
	 * @see GeneralMonstre.Direction
	 */
	private Direction directionAlea(Monstres m) {
		// Direction[] directionsBloques = new Direction[8];
		// int i=0;
		Direction[] directionAExplorer = new Direction[8];
		int j = 0;

		// On verifie qu'il n'est pas bloque dans une direction :
		int x = m.getPosition().getX() - carte.getOrigineX();
		int y = m.getPosition().getY() - carte.getOrigineY();

		// N
		if (!carte.visiteEstAccessible(y - 1, x)) {
			// directionsBloques[i++] = Direction.N;
		} else {
			/*
			 * NB : pour ameliorer on pourrais verifier ici verifier si on ne se dirige pas
			 * vers un bord de carte
			 * -> exploration inutile, ne va pas 'devoiler' d'autres cases a l'ordi
			 * et si directionAExplorer est vide, on choisi une direction qui n'est pas
			 * dans le tab directionsBloques
			 */
			directionAExplorer[j++] = Direction.N;
		}
		// NE
		if (carte.visiteEstAccessible(y - 1, x + 1)) {
			directionAExplorer[j++] = Direction.NE;
		}
		// E
		if (carte.visiteEstAccessible(y, x + 1)) {
			directionAExplorer[j++] = Direction.E;
		}
		// SE
		if (carte.visiteEstAccessible(y + 1, x + 1)) {
			directionAExplorer[j++] = Direction.SE;
		}
		// S
		if (carte.visiteEstAccessible(y + 1, x)) {
			directionAExplorer[j++] = Direction.S;
		}
		// SO
		if (carte.visiteEstAccessible(y + 1, x - 1)) {
			directionAExplorer[j++] = Direction.SE;
		}
		// O
		if (carte.visiteEstAccessible(y, x - 1)) {
			directionAExplorer[j++] = Direction.O;
		}
		// NO
		if (carte.visiteEstAccessible(y - 1, x - 1)) {
			directionAExplorer[j++] = Direction.NO;
		}
		System.out.print(" Les directions a explorer : ");
		for (int k = 0; k < j; k++) {
			System.out.print(directionAExplorer[k] + " ");
		}
		int d = (int) (Math.random() * j);
		System.out.print("\nOn explore la direction : " + directionAExplorer[d] + "\n");

		// On retourne une direction aleatoire :
		if (j != 0)
			return directionAExplorer[d];

		// bloque de tous les cotes, cas tres rare, le monstre ne peut pas avancer.
		return Direction.getDirectionAlea();
	}

	/**
	 * Renvoie une Position accessible par le monstre dans la direction donnee.
	 * 
	 * @param d : la direcction du deplacement du monstre
	 * @param m : le monstre
	 * @param i : l'indice du monstre
	 * @return
	 *         <ul>
	 *         <li>La position la plus loin dans la direction voulu</li>
	 *         <li>une position alea autour du monstre si une telle position n'a pas
	 *         ete trouvee</li>
	 *         </ul>
	 */
	private Position avanceMonstre(Direction d, Monstres m, int i) {
		int x, y;
		int n = m.getPortee() + 1;
		int dx = 0, dy = 0;

		// Indices dans le tableau carte.visite
		switch (d) {
			case N:
				dx = 0;
				dy = -1;
				break;
			case NE:
				dx = 1;
				dy = -1;
				break;
			case E:
				dx = 1;
				dy = 0;
				break;
			case SE:
				dx = 1;
				dy = 1;
				break;
			case S:
				dx = 0;
				dy = 1;
				break;
			case SO:
				dx = -1;
				dy = 1;
				break;
			case O:
				dx = -1;
				dy = 0;
				break;
			case NO:
				dx = -1;
				dx = -1;
				break;
		}

		/*
		 * On part de la pos du joueur et on
		 * se deplace vers la direction shouaitee
		 */
		x = m.getPosition().getX() - carte.getOrigineX();
		y = m.getPosition().getY() - carte.getOrigineY();

		// On se place a la position la plus eloigne accessible
		x += dx * m.getPortee();
		y += dy * m.getPortee();
		i = n - 1; // la portee du monstre
		while (i > 1 && !carte.visiteEstAccessible(y, x)) {
			x -= dx;
			y -= dy; // on recule si la case est pas accessible.
			i--;
		}
		// si pas de position trouvee on en cherche une aleatoirement
		if (!carte.visiteEstAccessible(y, x)) {
			return carte.trouvePositionVide(m.getPosition());
		}

		// sinon on renvoie la position trouvee :
		return new Position(y + carte.getOrigineY(), x + carte.getOrigineX());
	}

	/**
	 * Melange un tableau de Monstres.
	 * 
	 * @param troupes : Monstres[]
	 */
	private static void melangeTableau(Object[] troupes) {
		int i, j, n = troupes.length - 1;
		Object tempM;

		for (i = 0; i < n; i++) {

			/* on choisi un index aleatoirement */
			j = (int) (Math.random() * troupes.length);

			/*
			 * On echanges les valeurs du tableau
			 * aux indices i et j
			 */
			tempM = troupes[j];
			troupes[j] = troupes[i];
			troupes[i] = tempM;
		}
	}

}
