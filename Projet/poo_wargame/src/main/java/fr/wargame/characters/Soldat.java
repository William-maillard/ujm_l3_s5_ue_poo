package fr.wargame.characters;

import fr.wargame.map.Carte;
import fr.wargame.map.Element;
import fr.wargame.map.Position;
import fr.wargame.multimedia.SonPlayer;

/**
 * Represente un soldat pour le jeu wargame et implemente les actions possibles.
 **/
public abstract class Soldat extends Element implements ISoldat {

	private static final long serialVersionUID = 8823237592688089167L;
	private int pointsDeVie, puissanceFrappe, puissanceTir, portee;
	private int tour; /*
						 * valeurs : -1 : joue
						 * 0 : pas joue
						 * N>0 : incatif depuis N tours
						 */
	// private Thread tempT;
	private static SonPlayer soldatSon = new SonPlayer();

	Soldat(Carte carte, int pts, int portee, int puiss, int tir, Position pos) {
		super(pos);
		// NB : la carte sert a trouver une position vide
		pointsDeVie = pts;
		this.portee = portee;
		puissanceFrappe = puiss;
		puissanceTir = tir;
		tour = 0;
	}

	public int getTour() {
		return tour;
	}

	public void setTour(int b) {
		tour = b;
	}

	public void joueTour() {
		this.tour = -1;
		System.out.println("Soldat.joueTour()");
	}

	public void setPoints(int pointsDeVie) {
		this.pointsDeVie = pointsDeVie;
	}

	public int getPoints() {
		return pointsDeVie;
	}

	public int getPortee() {
		return portee;
	}

	public int getPuissanceFrappe() {
		return puissanceFrappe;
	}

	public int getTir() {
		return puissanceTir;
	}

	@Override
	public boolean estMort() {
		if (this.pointsDeVie <= 0) {
			soldatSon.wav("/death.wav", false);
			return true;
		}
		return false;
	}

	public void combat(Soldat soldat) {
		if (this.getTour() == 0) { // si soldat n'a pas encore joue
			if (this.getPosition().estVoisine(soldat.getPosition())) { // corps a corps

				soldatSon.wav("/cac.wav", false);

				if (!this.estMort()) {
					int randomFrappe = (int) ((Math.random() * puissanceFrappe)) + 1;
					soldat.pointsDeVie -= randomFrappe;
				}
				if (!soldat.estMort()) {
					int randomFrappe = (int) ((Math.random() * soldat.puissanceFrappe)) + 1;
					pointsDeVie -= randomFrappe;
				}
			} else if (this.getPosition().estVoisine(soldat.getPosition(), this.getPortee())) { /* combat a distance */
				if (puissanceTir != 0) { // il sait tirer

					soldatSon.wav("/distance.wav", false);

					if (soldat.puissanceTir >= this.puissanceTir) { // si les deux soldat pouvent tirer a la meme
																	// distance
						if (!this.estMort()) {
							int randomTir = (int) ((Math.random() * this.puissanceTir)) + 1;
							soldat.pointsDeVie -= randomTir;
						}
						if (!soldat.estMort()) {
							int randomTir = (int) ((Math.random() * soldat.puissanceTir)) + 1;
							this.pointsDeVie -= randomTir;
						}
					} else { // sinon attaque une fois
						int randomTir = (int) ((Math.random() * this.puissanceTir)) + 1;
						soldat.pointsDeVie -= randomTir;
					}
				}
			}
			this.joueTour(); // soldat courant a jouee !
		}
	}

	public void seDeplace(Position newPos) {
		this.setPosition(newPos);
		this.joueTour();
	}

	@Override
	public String toString() {
		String s = "";

		if (this instanceof Heros) {
			Heros h = ((Heros) this);
			s += super.toString() + "<ul>";
			s += "<li>PV : " + pointsDeVie + " / " + h.getType().getPoints() + "</li>";
		} else { // this instanceof Monstres
			Monstres m = ((Monstres) this);

			s += super.toString() + "<ul>";
			s += "<li>PV : " + pointsDeVie + " / " + m.getType().getPoints() + "</li>";
		}
		s += "<li>" + "Portee : " + portee + "</li>";
		s += "<li>" + "Attaque : " + "<ul><li>corps-a-corps = " + puissanceFrappe + "</li><li>distance = "
				+ puissanceTir + "</li></ul></li></ul>";

		return s;
	}
}
