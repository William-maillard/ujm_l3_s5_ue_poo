package fr.wargame.characters;

import java.awt.Graphics;

import fr.wargame.graphic.Animation;
import fr.wargame.map.Carte;
import fr.wargame.map.Position;

/** Represente les Heros du jeu **/
public class Heros extends Soldat {

	private static final long serialVersionUID = 6109140513274354760L;
	private final TypesH TYPE;
	private final char NOM;

	/**
	 * Instancie un Heros.
	 * 
	 * @param carte : celle du jeu,
	 * @param type  : le type de heros a creer,
	 * @param nom   : le nom du heros,
	 * @param pos   : la position du heros.
	 */
	public Heros(Carte carte, TypesH type, char nom, Position pos) {
		super(carte, type.getPoints(), type.getPortee(), type.getPuissance(), type.getTir(), pos);
		NOM = nom;
		TYPE = type;
		loadAnimation();
	}

	/**
	 * Permet de donner une animation au Heros,
	 * notament apres le chargement d'une partie
	 * car l'annimation n'est pas Serializable.
	 */
	public void loadAnimation() {

		switch (TYPE) {
			case HUMAIN:
				animation = new Animation(sprite.alchemistWalk, sprite.alchemistIdle, 4);
				break;
			case NAIN:
				animation = new Animation(sprite.butcherWalk, sprite.butcherIdle, 4);
				break;
			case ELF:
				animation = new Animation(sprite.archerWalk, sprite.archerIdle, 4);
				break;
			case HOBBIT:
				animation = new Animation(sprite.thiefWalk, sprite.thiefIdle, 4);
				break;
		}
		animation.idle();
	}

	public TypesH getType() {
		return TYPE;
	}

	public Animation getAnimation() {
		return animation;
	}

	@Override
	public void seDessiner(Graphics g) {
		int x = getPosition().getX() * NB_PIX_CASE;
		int y = getPosition().getY() * NB_PIX_CASE;
		// pour laisser la bordure apparente
		x++;
		y++;

		animation.seDessiner(g, x, y, this.getTour());

	}

	/**
	 * Dessiner un heros a une position donnee
	 * 
	 * @param g le context graphique
	 * @param x l'abssysse
	 * @param y l'ordonee
	 */
	public void seDessiner(Graphics g, int x, int y) {
		x *= NB_PIX_CASE;
		y *= NB_PIX_CASE;
		// pour laisser la bordure apparente
		x++;
		y++;

		animation.seDessiner(g, x, y, this.getTour());

	}

	@Override
	public String toString() {
		return "<h1>" + TYPE + " " + NOM + "</h1> " + super.toString();
	}

}
