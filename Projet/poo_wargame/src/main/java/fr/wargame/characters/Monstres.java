package fr.wargame.characters;

import java.awt.Graphics;

import fr.wargame.graphic.Animation;
import fr.wargame.map.Carte;
import fr.wargame.map.Position;

/**
 * Represente les ennemis du jeu controles par l'ordinateur
 * 
 * @see GeneralMonstre
 **/
public class Monstres extends Soldat {

	private static final long serialVersionUID = -7846058332112213993L;
	private final TypesM TYPE;
	private final int NOM;

	public Monstres(Carte carte, TypesM type, int nom, Position pos) {
		super(carte, type.getPoints(), type.getPortee(), type.getPuissance(), type.getTir(), pos);
		TYPE = type;
		NOM = nom;
		loadAnimation();

	}

	/**
	 * Permet de donner une animation au Monstres,
	 * notament apres le chargement d'une partie
	 * car l'annimation n'est pas Serializable.
	 */
	public void loadAnimation() {

		switch (TYPE) {
			case ORC:
				animation = new Animation(sprite.largeKnightWalk, sprite.largeKnightIdle, 4);
				break;
			case TROLL:
				animation = new Animation(sprite.heavyKnightWalk, sprite.heavyKnightIdle, 4);
				break;
			case GOBELIN:
				animation = new Animation(sprite.knightWalk, sprite.knightIdle, 4);
				break;
		}
		animation.idle();
	}

	public TypesM getType() {
		return TYPE;
	}

	@Override
	public void seDessiner(Graphics g) {
		int x = getPosition().getX() * NB_PIX_CASE;
		int y = getPosition().getY() * NB_PIX_CASE;
		// pour laisser la bordure apparente
		x++;
		y++;

		animation.seDessiner(g, x, y, this.getTour());
	}

	@Override
	public String toString() {
		return "<h1>" + TYPE + " " + NOM + "</h1> " + super.toString();
	}
}
