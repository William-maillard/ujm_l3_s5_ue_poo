package fr.wargame;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

import fr.wargame.graphic.PanneauJeu;
import fr.wargame.map.Carte;
import fr.wargame.multimedia.SonPlayer;

/**
 * Cette classe implemente la boucle principale du jeu wargame et a
 * ete realise avec l'aide du lien ci dessous.
 * Elle permet de partager le temps entre :
 * <ul>
 * <li>raffraichir l'affichage a une frequence definis (fps),</li>
 * <li>verifier que le jeu n'est pas termine.</li>
 * </ul>
 * 
 * <a href="https://dewitters.com/dewitters-gameloop/">lien source</a>
 */
public class BoucleJeu extends Thread {

	private static PanneauJeu affichage;
	private static Carte carte;
	// private static int nbTours = 0;
	/** Musique de fond du jeu **/
	private static SonPlayer bgMusique;
	public static boolean tourJ1 = true;
	/** pour pouvoir arreter le thread du jeu **/
	public static Thread currentThread = null;

	/** pour arreter ou non la boucle **/
	private boolean running = true;

	/** pour 60s ups (update per second) **/
	private final double updateRate = 1.0d / 60.0d;

	public BoucleJeu() {
		// pour la musique du jeu
		// bgMusique = new SonPlayer();
		// bgMusique.midi("/1.mid", true); // demare la musique

		try {
			ObjectInputStream ois = new ObjectInputStream(
					getClass().getClassLoader().getResourceAsStream("carte_1.ser"));
			carte = (Carte) ois.readObject();
			ois.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("Erreur sauvegarde : file not found");
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("Erreur sauvegarde : IO exception");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}

		carte.loadAnimation();
		affichage = new PanneauJeu(carte);
		tourJ1 = true;
	}

	public BoucleJeu(String sauvegarde) {
		// pour la musique du jeu
		bgMusique = new SonPlayer();
		bgMusique.midi("/1.mid", true);

		try {
			System.out.println("Chargement de la carte : " + sauvegarde);
			// ObjectInputStream ois = new
			// ObjectInputStream(getClass().getClassLoader().getResourceAsStream(sauvegarde));
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(".sauv/" + sauvegarde));
			carte = (Carte) ois.readObject();
			ois.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.out.println("Erreur chargement : file not found");
			return;
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("Erreur chargement : IO exception");
			return;
		} catch (ClassNotFoundException e1) {
			System.out.println("Erreur chargement : classe introuvable.");
			e1.printStackTrace();
			return;
		} finally {
			// il faut retourner au menu du jeu ?
			if (carte == null) {
				System.err.println("Erreur de chargement de la carte. Fin du programme.");
				System.exit(-1);
			}
		}
		carte.loadAnimation();
		affichage = new PanneauJeu(carte);
		String cheminImage = sauvegarde.substring(0, sauvegarde.length() - 4) + ".png";
		System.out.println("chemin " + cheminImage);
		affichage.chargerImageCarte(cheminImage);
		tourJ1 = true;
	}

	@Override
	public void run() {
		BoucleJeu.currentThread = Thread.currentThread();
		double acc = 0;
		long currentTime, lastUpdate = System.currentTimeMillis();
		double lastRenderTimeInSeconds;

		while (running) {
			if (Thread.currentThread().isInterrupted()) {
				interrupt();
			}
			currentTime = System.currentTimeMillis();
			lastRenderTimeInSeconds = (currentTime - lastUpdate) / 1000d;

			// acc pour savoir combien de temps une update prend
			acc += lastRenderTimeInSeconds;
			lastUpdate = currentTime;

			while (acc > updateRate) {
				update();

				// dans le cas ou l'etape d'affichage prend bc de temps
				acc -= updateRate;
			}
			render();
			// pour laisser du temps au cpu de liberer la memoire
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				interrupt();
				e.printStackTrace();
			}

		}

	}

	/** actualise l'affichage du jeu **/
	private void render() {
		affichage.repaint();
	}

	/** actualise l'etat du jeu **/
	private void update() {
		// boucle classique
		if (carte.fin() != 0) {
			running = false; // stop la boucle de jeu.
			affichage.getFenetre().dispose();
		}
		if (tourJ1 == false) {
			System.out.println("Fin du tour du joueur !");
			carte.finTourHeros();
			carte.generalMonstre.jouerTour();
			System.out.println("Fin du tour de l'ennemi !");
			carte.finTourMonstres();
			// nbTours += 1;
			tourJ1 = true;
		}
	}

	public void interrupt() {
		bgMusique.stop();
		this.running = false;
	}

}
