package fr.wargame.graphic;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Permet de creer une Animation pour les Soldats.
 * Chaque animation est compose de sprites au repose
 * et en mouvement.
 * @see SpriteChargement
 *
 */
public class Animation {
	
	/** index de l'image actuelle dans le tableau d'images de l'animation  **/
	private int index;
	/** vitesse de rafraichissement                                        **/
	private int vitesse;

	/** images pour l'animation au repos                                   **/
	private BufferedImage[] idle;
	/** images pour animation marche                                       **/
	private BufferedImage[] walk;
	//image a afficher
	private BufferedImage imageActuelle;
	private static AlphaComposite opaqueComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f);
	private static AlphaComposite normalComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1);

	
	private Timer timer;
	
	
	/**
	 * Permet de creer une Animation pour un Soldat.
	 * @param walk : la liste des images pour l'animation en mouvement.
	 * @param idle : la liste des images pour l'animation au repos.
	 * @param vitesse : celle de l'animation.
	 */
	public Animation(BufferedImage[] walk, BufferedImage[] idle, int vitesse) {
		this.walk = walk;
		this.idle = idle;
		this.index = 0;
		this.vitesse = vitesse;
		
		timer = new Timer();		
	}
	
	
	/** @return Image : l'image actuelle de l'aanimation               **/
	public BufferedImage getImageActuelle() {return imageActuelle; };
	
	/** Permet de demarrer l'animation 'en mouvement' en creant  
	    un timer qui change d'image toutes les 1000ms / vitesse,       **/
	public void walking() {
		index = 0;
		timer.scheduleAtFixedRate(
			new TimerTask(){	
				@Override
				public void run() {
					imageActuelle = walk[index];
					index += 1;
					index = index % walk.length;
				}
			}
		, 0, 1000 / vitesse);
	}
	
	/** Permet de demarrer l'animation 'au repos' en creant 
    	un timer qui change d'image toutes les 1000ms / vitesse,       **/
	public void idle() {
		index = 0;
		timer.scheduleAtFixedRate(
			new TimerTask(){	
				@Override
				public void run() {
					imageActuelle = idle[index];
					index += 1;
					index = index % idle.length;
				}
			}
		, 0, 1000 / vitesse);
	}
	
	/** Arrete l'animation courante (detruit le timer)                 **/
	public void stop() {
		timer.cancel();
		timer = new Timer();
	}
	
	/**
	 * Permet de dessiner l'imageActuelle de l'animation.
	 * @param g : contexte graphique sur lequel dessiner l'image.
	 * @param x : abscysse
	 * @param y : ordonnee
	 * @param aJoue : permet de savoir si le heros a deja joue, il est alors dessine en transparence.
	 */
	public void seDessiner(Graphics g, int x, int y, int aJoue) {
        Graphics2D g2d = (Graphics2D) g;
        
        if(aJoue != -1) {
        	g2d.drawImage(imageActuelle, x, y, null);
        }
        else {        	
        	g2d.setComposite(opaqueComposite);
        	g2d.drawImage(imageActuelle, x, y, null);
        	g2d.setComposite(normalComposite);        
        }
	}
}
