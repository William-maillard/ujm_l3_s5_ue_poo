package fr.wargame.graphic;

import javax.imageio.ImageIO;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.wargame.FenetreJeu;
import fr.wargame.IConfig;
import fr.wargame.characters.Heros;
import fr.wargame.characters.Monstres;
import fr.wargame.map.Carte;
import fr.wargame.map.Element;
import fr.wargame.map.Obstacle;
import fr.wargame.map.Position;
import fr.wargame.multimedia.ChargementRessourcesMenu;

import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.awt.Robot;
import java.awt.Rectangle;

/** Permet de lancer le mode 'creation de carte'. **/
public class PanneauCreation extends JPanel implements IConfig {

	private static final long serialVersionUID = -1063225132783240860L;

	/* ----- Les composants graphique de la fenetre ----- */
	private JFrame fenetre;
	private JPanel infos;

	/* ----- Attributs pour creer la carte ----- */
	private int nbHeros, nbMonstres, nbObstacles;
	private HashMap<Position, Element> carte;
	private LinkedList<Heros> heros;
	private LinkedList<Monstres> monstres;
	private LinkedList<Obstacle> obstacles;

	/* ----- Pour la sauvegarde ----- */
	// private Carte objetCarte;
	private boolean captureEcran = false;

	/* ----- Les sprites de la carte ----- */
	public BufferedImage spritesCarte;
	private Image[] imagesObstacles;
	private Image[] imagesVide;

	/* ----- Pour afficher les elements de la carte----- */
	private int dx = 50; // decallage de la carte par rapport a l'axe des abscysses
	private int[][] elements;
	private ChargementRessourcesMenu images;
	private int n = (LARGEUR_CARTE * NB_PIX_CASE + dx); /*
														 * pour pas reclaculer la valeur a chaque fois
														 * dans l'ecouteur d'evenements
														 */
	private boolean imageSelectionnee = false;
	private boolean imageClique = false;
	private int numImage;
	private Position posImage = new Position(0, 0);

	public PanneauCreation(ChargementRessourcesMenu images) {
		/* -- Initialisation -- */
		creerFenetreJeu();
		nbHeros = 0;
		nbMonstres = 0;
		carte = new HashMap<Position, Element>(NB_ELEMENTS);
		heros = new LinkedList<Heros>();
		monstres = new LinkedList<Monstres>();
		obstacles = new LinkedList<Obstacle>();
		elements = new int[LARGEUR_CARTE][HAUTEUR_CARTE];
		this.images = images;
		imagesObstacles = new Image[4];
		imagesVide = new Image[4];
		chargeImages();

		/* -- Ecouteurs d'evenements -- */

		MouseAdapter ma = creerEcouteurEvent();

		this.addMouseMotionListener(ma);
		this.addMouseListener(ma);
	}

	/**
	 * Cree la fenetre principale du jeu.
	 */
	private void creerFenetreJeu() {
		fenetre = new JFrame("Wargame- Creez votre propre carte de jeu.");
		// fenetre.setUndecorated(true);

		infos = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawString(
						"Heros : " + nbHeros + "/" + NB_HEROS +
								", Monstres : " + nbMonstres + "/" + NB_MONSTRES +
								", Obstacles : " + nbObstacles + "/ [" + (NB_OBSTACLES / 2) + ", " + NB_OBSTACLES + "]",
						0, 20);
			}
		};

		/* ----- Creation d'un bouton pour sauvegarder la carte ---- */
		JButton sauvegarde = new JButton("Sauvegarde");
		infos.add(sauvegarde);
		sauvegarde.setBackground(Color.BLUE.darker().darker());
		sauvegarde.setSize(20, 20);

		sauvegarde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*
				 * if(nbHeros < NB_HEROS || nbMonstres < NB_MONSTRES ||
				 * nbObstacles<NB_OBSTACLES/2) {
				 * return; // faire un message de dialogue ?
				 * }
				 */
				/* -- On cree la carte -- */
				BitSet[] elem = new BitSet[HAUTEUR_CARTE];
				for (int i = 0; i < HAUTEUR_CARTE; i++) {
					elem[i] = new BitSet(LARGEUR_CARTE);
				}
				/*
				 * on verifie que toutes les cases sont remplis :
				 * sinon on met de la terre
				 */
				int i, j;
				for (i = 0; i < LARGEUR_CARTE; i++) {
					for (j = 0; j < HAUTEUR_CARTE; j++) {
						if (elements[i][j] == 0) {
							elements[i][j] = 13;
						}
					}
				}
				// On supprime l'affichage des monstre :
				captureEcran = true;
				PanneauCreation.this.repaint();

				/* -- On demande un nom a l'utilisateur -- */
				int tmps;
				int erreur = 0;
				String chemin = JOptionPane.showInputDialog(null, "donnez un nom a votre carte :");

				if (chemin != null) {

					/*
					 * On verifie que l'utilisateur n'a pas donne de chemin.
					 * Si c'est le cas on garde juste le nom en bout de chemin
					 */
					tmps = chemin.lastIndexOf(File.pathSeparator);
					if (tmps != -1) {
						chemin = chemin.substring(tmps);
					}
					/* On rajoute le chemin pour le dossier de sauvegarde + l'extension */
					String formatCarte = "png";
					String cheminImageCarte = DIR_SAUVEGARDE + "images/" + chemin + "." + formatCarte;
					// chemin = ".sauv/" + chemin + ".ser";
					chemin = DIR_SAUVEGARDE + chemin + EXTENSION_SAUVEGARDE;

					/* On test si le fichier existe, puis sauvegarde la Partie */
					File fichier = new File(chemin);
					if (fichier.exists()) {
						erreur = JOptionPane.showConfirmDialog(null,
								"Une carte du meme nom existe, voulez vous l'ecraser ?", "Sauvegarde",
								JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
					}
					if (erreur == 1)
						return;

					try {
						/* -- Serialisation de la carte -- */
						ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(chemin));
						oos.writeObject(new Carte(carte, heros, monstres, elem));
						oos.close();

						/* -- Capure de l'image de la carte -- */

						// on prend une capture d'ecran :
						Robot robot = new Robot();
						Point p = fenetre.getLocation(); // location de la fenetre sur l'ecran
						// rectangle de la capture a effectuer
						Rectangle rectangleCapture = new Rectangle((int) p.getX() + dx + 7, (int) p.getY() + 65,
								LARGEUR_CARTE * NB_PIX_CASE, HAUTEUR_CARTE * NB_PIX_CASE);
						BufferedImage imageCarte = robot.createScreenCapture(rectangleCapture);
						File f = new File(cheminImageCarte);
						ImageIO.write(imageCarte, formatCarte, f); /*
																	 * attention changer le new File
																	 * qui va pas marcher dans le jar executable
																	 */

					} catch (AWTException e1) {
						System.err.println("Erreur d'instanciation du robot.");
						erreur++;
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
						System.out.println("Erreur sauvegarde : file not found");
						erreur++;
					} catch (IOException e1) {
						e1.printStackTrace();
						System.out.println("Erreur sauvegarde : IO exception");
						erreur++;
					} catch (SecurityException e1) {
						System.err.println("PBM de droits...");
					} finally {
						JOptionPane.showMessageDialog(
								null,
								"Sauvegarde " + ((erreur == 0) ? "effectue avec succes." : "non effectuee."),
								"Resultat sauvegarde",
								(erreur == 0) ? JOptionPane.INFORMATION_MESSAGE : JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});

		/* ----- Creation d'un bouton pour recommencer ----- */
		JButton recommencer = new JButton("Recommencer");
		infos.add(recommencer);
		recommencer.setBackground(Color.GRAY);
		recommencer.setSize(20, 20);

		recommencer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// On reinitialise les attributs :
				for (int i = 0; i < LARGEUR_CARTE; i++) {
					Arrays.fill(elements[i], 0);
				}
				nbHeros = nbMonstres = nbObstacles = 0;
				heros.clear();
				monstres.clear();
				obstacles.clear();
				carte.clear();

				// On actualise l'affichage :
				PanneauCreation.this.repaint();
			}
		});

		/* ----- Creation d'un bouton pour retourner au menu ----- */
		JButton quitter = new JButton("Quitter");
		infos.add(quitter);
		quitter.setBackground(Color.RED.darker());
		quitter.setSize(20, 20);

		quitter.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int choix = JOptionPane.showConfirmDialog(null, "Voulez vous quitter le mode creation ?", "Quitter",
						JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

				if (choix == 0) { // on quitte le mode et on va dans le menu.
					new Thread(new Thread() {
						public void run() {
							try {
								FenetreJeu.main(null);
							} catch (MidiUnavailableException | IOException | InvalidMidiDataException e) {
								System.err.println("Erreur de chargment du menu pour quitter le mode creation");
								e.printStackTrace();
							}
						}
					}).start();
					fenetre.dispose();
				}
			}
		});

		/* ----- Ajout des composants a la fenetre ----- */
		fenetre.getContentPane().setLayout(new BorderLayout());
		fenetre.getContentPane().add(this, BorderLayout.CENTER);
		fenetre.getContentPane().add(infos, BorderLayout.NORTH);

		/* ----- Parametrage de la fenetre ----- */
		fenetre.setLocation(POSITION_X, POSITION_Y);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// fenetre.setResizable(false);
		fenetre.setSize((LARGEUR_CARTE + 1) * NB_PIX_CASE + 100, (HAUTEUR_CARTE + 1) * NB_PIX_CASE + 50);
		fenetre.setVisible(true);
	}

	private void chargeImages() {
		try {
			spritesCarte = ImageIO.read(getClass().getClassLoader().getResource("spritesCarte.png"));
		} catch (IOException e) {
			System.err.println("Erreur de chargement de la feuille de sprite de la carte.");
			return;
		}
		// panier de piments
		imagesObstacles[0] = spritesCarte.getSubimage(385, 298, 32, 32);
		// botte de foin
		imagesObstacles[1] = spritesCarte.getSubimage(352, 257, 32, 32);
		// tas de bois
		imagesObstacles[2] = spritesCarte.getSubimage(130, 320, 32, 32);
		// eau
		imagesObstacles[3] = spritesCarte.getSubimage(95, 320, 32, 32);

		// Herbe
		imagesVide[0] = spritesCarte.getSubimage(175, 5, 32, 32);
		// terre
		imagesVide[1] = spritesCarte.getSubimage(225, 32, 32, 32);
		// gravier
		imagesVide[2] = spritesCarte.getSubimage(134, 32, 32, 32);
		// pont
		imagesVide[3] = spritesCarte.getSubimage(385, 95, 32, 32);

		/* Plus besoin de la feuille de sprite */
		spritesCarte = null;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		/*
		 * NB : on repere les images par un entier
		 * correspondant a l'odre dans lequel elles
		 * sont dessinees
		 */

		/* -- On dessine les images sur le cote gauche -- */
		int x = 10, y = 20;
		g.setFont(POLICE_LEGENDE);
		g.setColor(COULEUR_TEXTE);
		g.drawString("H", x + 3, y);
		y += 5;
		g.drawImage(images.alchemist, x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(images.butcher, x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(images.archer, x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(images.thief, x, y, null);
		y += NB_PIX_CASE + 50;

		g.drawString("M", x + 2, y);
		y += 10;
		g.drawImage(images.largeKnight, x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(images.heavyKnight, x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(images.eliteKnight, x, y, null);
		y += NB_PIX_CASE + 10;

		/* -- Dessin de la grille de la carte -- */
		g.setColor(COULEUR_TEXTE);
		int i;
		x = 50;
		for (i = 0; i <= LARGEUR_CARTE; i++) {
			// ligne verticale
			g.drawLine(x, 0, x, HAUTEUR_CARTE * NB_PIX_CASE);
			x += NB_PIX_CASE;
		}
		x = 50;
		y = 0;
		for (i = 0; i <= HAUTEUR_CARTE; i++) {
			// ligne horizontale
			g.drawLine(x, y, x + LARGEUR_CARTE * NB_PIX_CASE, y);
			y += NB_PIX_CASE;
		}

		/* -- On dessine les images sur la droite (les sprites de la carte) -- */
		x += LARGEUR_CARTE * NB_PIX_CASE + 10;
		y = 20;
		g.drawString("O", x + 4, y);
		y += 5;
		g.drawImage(imagesObstacles[0], x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(imagesObstacles[1], x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(imagesObstacles[2], x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(imagesObstacles[3], x, y, null);
		y += NB_PIX_CASE + 50;

		g.drawString("C", x + 4, y);
		y += 10;
		g.drawImage(imagesVide[0], x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(imagesVide[1], x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(imagesVide[2], x, y, null);
		y += NB_PIX_CASE + 10;
		g.drawImage(imagesVide[3], x, y, null);

		/* -- On dessine la carte -- */
		for (x = 0; x < LARGEUR_CARTE; x++) {
			for (y = 0; y < HAUTEUR_CARTE; y++) {
				if (elements[x][y] != 0) {
					g.drawImage(image(elements[x][y]), x * NB_PIX_CASE + dx, y * NB_PIX_CASE, null);
				}
			}
		}
		/* -- On dessine les Elements -- */
		if (!captureEcran) {
			for (Heros h : heros) {
				g.drawImage(image(h.getType()), h.getPosition().getX() * NB_PIX_CASE + dx,
						h.getPosition().getY() * NB_PIX_CASE, null);
			}
			for (Monstres m : monstres) {
				g.drawImage(image(m.getType()), m.getPosition().getX() * NB_PIX_CASE + dx,
						m.getPosition().getY() * NB_PIX_CASE, null);
			}
		}
		for (Obstacle o : obstacles) {
			g.drawImage(image(o.getType()), o.getPosition().getX() * NB_PIX_CASE + dx,
					o.getPosition().getY() * NB_PIX_CASE, null);
		}

		/* -- si on drag une image -- */
		if (imageSelectionnee) {
			g.drawImage(image(numImage), posImage.getX(), posImage.getY(), null);
		}

		infos.repaint();
	}

	private MouseAdapter creerEcouteurEvent() {
		return new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				int x = e.getX(), y = e.getY();

				// clique a gauche :
				if (x >= 10 && x <= 42) {

					// Images Heros
					if (y >= 25 && y <= 57) {
						imageSelectionnee = true;
						numImage = 1;
					} else if (y >= 67 && y <= 99) {
						imageSelectionnee = true;
						numImage = 2;
					} else if (y >= 109 && y <= 141) {
						imageSelectionnee = true;
						numImage = 3;

					} else if (y >= 151 && y <= 183) {
						imageSelectionnee = true;
						numImage = 4;
					}

					// Images Monstres
					else if (y >= 243 && y <= 275) {
						imageSelectionnee = true;
						numImage = 5;
					} else if (y >= 285 && y <= 317) {
						imageSelectionnee = true;
						numImage = 6;
					} else if (y >= 327 && y <= 359) {
						imageSelectionnee = true;
						numImage = 7;
					}
				}
				// clique a droite
				else if (x >= n && x <= n + 32) {
					// Images Obstacles
					if (y >= 25 && y <= 57) {
						imageSelectionnee = true;
						numImage = 8;
					} else if (y >= 67 && y <= 99) {
						imageSelectionnee = true;
						numImage = 9;
					} else if (y >= 109 && y <= 141) {
						imageSelectionnee = true;
						numImage = 10;
					} else if (y >= 151 && y <= 183) {
						imageSelectionnee = true;
						numImage = 11;
					}

					// Images Vide
					else if (y >= 243 && y <= 275) {
						imageSelectionnee = true;
						numImage = 12;
					} else if (y >= 285 && y <= 317) {
						imageSelectionnee = true;
						numImage = 13;
					} else if (y >= 327 && y <= 359) {
						imageSelectionnee = true;
						numImage = 14;
					} else if (y >= 369 && y <= 401) {
						imageSelectionnee = true;
						numImage = 15;
					}
				}
				if (imageSelectionnee) {
					posImage.setX(x);
					posImage.setY(y);
					imageClique = false;
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (imageSelectionnee) {
					int x = e.getX(), y = e.getY();
					imageSelectionnee = false;
					// a-t-on 'drop' l'image sur la carte ?
					if (x >= dx && x <= LARGEUR_CARTE * NB_PIX_CASE) {
						// on ajoute l'image dans le tableau :
						if (numImage > 11) {
							elements[(x - dx) / NB_PIX_CASE][y / NB_PIX_CASE] = numImage;
						}
						if (numImage <= 11) {
							// il faut ajouter dans la HashMap
							ajouterElement(numImage, (x - dx) / NB_PIX_CASE, y / NB_PIX_CASE);
						}
						PanneauCreation.this.repaint();
					}
				}
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				int x = e.getX(), y = e.getY();
				// clique a gauche :
				if (x >= 10 && x <= 42) {

					// Images Heros
					if (y >= 25 && y <= 57) {
						imageClique = true;
						numImage = 1;
					} else if (y >= 67 && y <= 99) {
						imageClique = true;
						numImage = 2;
					} else if (y >= 109 && y <= 141) {
						imageClique = true;
						numImage = 3;

					} else if (y >= 151 && y <= 183) {
						imageClique = true;
						numImage = 4;
					}

					// Images Monstres
					else if (y >= 243 && y <= 275) {
						imageClique = true;
						numImage = 5;
					} else if (y >= 285 && y <= 317) {
						imageClique = true;
						numImage = 6;
					} else if (y >= 327 && y <= 359) {
						imageClique = true;
						numImage = 7;
					}
				}
				// clique a droite
				else if (x >= n && x <= n + 32) {
					// Images Obstacles
					if (y >= 25 && y <= 57) {
						imageClique = true;
						numImage = 8;
					} else if (y >= 67 && y <= 99) {
						imageClique = true;
						numImage = 9;
					} else if (y >= 109 && y <= 141) {
						imageClique = true;
						numImage = 10;
					} else if (y >= 151 && y <= 183) {
						imageClique = true;
						numImage = 11;
					}

					// Images Vide
					else if (y >= 243 && y <= 275) {
						imageClique = true;
						numImage = 12;
					} else if (y >= 285 && y <= 317) {
						imageClique = true;
						numImage = 13;
					} else if (y >= 327 && y <= 359) {
						imageClique = true;
						numImage = 14;
					} else if (y >= 369 && y <= 401) {
						imageClique = true;
						numImage = 15;
					}
				} else if (x >= dx && x <= LARGEUR_CARTE * NB_PIX_CASE) {
					// on ajoute l'image dans le tableau :
					if (numImage > 11) {
						elements[(x - dx) / NB_PIX_CASE][y / NB_PIX_CASE] = numImage;
					} else {
						// il faut ajouter dans la HashMap
						ajouterElement(numImage, (x - dx) / NB_PIX_CASE, y / NB_PIX_CASE);
					}
					PanneauCreation.this.repaint();
				}
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				if (imageSelectionnee) {
					posImage.setX(e.getX());
					posImage.setY(e.getY());
					PanneauCreation.this.repaint();
				}
				if (imageClique && numImage > 7 &&
						e.getX() >= dx && e.getX() < n &&
						e.getY() >= 0 && e.getY() < HAUTEUR_CARTE * NB_PIX_CASE) {
					// on ajoute l'image dans le tableau :
					if (numImage > 11) {
						elements[(e.getX() - dx) / NB_PIX_CASE][e.getY() / NB_PIX_CASE] = numImage;
					} else {
						// il faut ajouter dans la HashMap
						ajouterElement(numImage, (e.getX() - dx) / NB_PIX_CASE, e.getY() / NB_PIX_CASE);
					}
					PanneauCreation.this.repaint();
				}
			}
		};
	}

	/**
	 * Fait correspondre un entier a une image.
	 * 
	 * @param i : le numero de l'image.
	 * @return l'image.
	 */
	private Image image(int i) {
		switch (i) {
			case 1:
				return images.alchemist;
			case 2:
				return images.butcher;
			case 3:
				return images.archer;
			case 4:
				return images.thief;
			case 5:
				return images.largeKnight;
			case 6:
				return images.heavyKnight;
			case 7:
				return images.eliteKnight;
			case 8:
				return imagesObstacles[0];
			case 9:
				return imagesObstacles[1];
			case 10:
				return imagesObstacles[2];
			case 11:
				return imagesObstacles[3];
			case 12:
				return imagesVide[0];
			case 13:
				return imagesVide[1];
			case 14:
				return imagesVide[2];
			default /* 15 */:
				return imagesVide[3];

		}
	}

	/**
	 * Fait correspondre un type de heros a une image.
	 * 
	 * @param type
	 * @return
	 */
	private Image image(Heros.TypesH type) {
		switch (type) {
			case HUMAIN:
				return image(1);
			case NAIN:
				return image(2);
			case ELF:
				return image(3);
			default /* HOBBIT */:
				return image(4);
		}
	}

	private Image image(Monstres.TypesM type) {
		switch (type) {
			case ORC:
				return image(5);
			case TROLL:
				return image(6);
			default /* GOBELIN */:
				return image(7);
		}
	}

	private Image image(Obstacle.TypeObstacle type) {
		switch (type) {
			case BOIS:
				return image(8);
			case FOIN:
				return image(9);
			case PANIER:
				return image(10);
			default /* EAU */:
				return image(11);
		}
	}

	/**
	 * Ajoute un Element dans la Map et increment le conteur
	 * (Heros, Monstres, Obstacles)
	 * 
	 * @param i : numero de l'image [1-11]
	 * @param x : abscysse dans la carte ou placer l'element
	 * @param y : ordonnee dans la carte ou placer l'element
	 * @return
	 *         <ul>
	 *         <li>true si l'element a pu etre ajoute</li>
	 *         <li>false sinon</li>
	 *         </ul>
	 */
	private boolean ajouterElement(int i, int x, int y) {
		Position p = new Position(y, x);

		if (i <= 4) { // Hero
			if (nbHeros == NB_HEROS) {
				return false;
			}
			Heros.TypesH type;
			switch (i) {
				case 1:
					type = Heros.TypesH.HUMAIN;
					break;
				case 2:
					type = Heros.TypesH.NAIN;
					break;
				case 3:
					type = Heros.TypesH.ELF;
					break;
				default /* 4 */:
					type = Heros.TypesH.HOBBIT;
					break;
			}
			Heros h = new Heros(null, type, (char) ('a' + nbHeros), p);
			heros.add(h);
			Element e = carte.put(p, h);
			supprimeElement(e); // on suprime des listes si c'est un element
			nbHeros++;
		} else if (i <= 7) { // Monstre
			if (nbMonstres == NB_MONSTRES) {
				return false;
			}
			Monstres.TypesM type;
			switch (i) {
				case 5:
					type = Monstres.TypesM.ORC;
					break;
				case 6:
					type = Monstres.TypesM.TROLL;
					break;
				default /* 7 */:
					type = Monstres.TypesM.GOBELIN;
					break;
			}
			Monstres m = new Monstres(null, type, nbMonstres, p);
			monstres.add(m);
			Element e = carte.put(p, m);
			supprimeElement(e); // on suprime des listes
			nbMonstres++;
		} else { // Obstacle
			if (nbObstacles == NB_OBSTACLES) {
				return false;
			}
			Obstacle.TypeObstacle type;
			switch (i) {
				case 8:
					type = Obstacle.TypeObstacle.PANIER;
					break;
				case 9:
					type = Obstacle.TypeObstacle.FOIN;
					break;
				case 10:
					type = Obstacle.TypeObstacle.BOIS;
					break;
				default /* 11 */:
					type = Obstacle.TypeObstacle.EAU;
					break;
			}
			Obstacle o = new Obstacle(type, p);
			obstacles.add(o);
			Element e = carte.put(p, o);
			supprimeElement(e);
			nbObstacles++;
		}

		return true;
	}

	/**
	 * Permet de supprimer des listes heros ou monstre
	 * losqu'ils sont supprimes de la carte.
	 * 
	 * @param e
	 */
	private void supprimeElement(Element e) {
		if (e instanceof Element) { // On a remplace un Element existant
			if (e instanceof Heros) {
				heros.remove((Heros) e);
				nbHeros--;
			} else if (e instanceof Monstres) {
				monstres.remove((Monstres) e);
				nbMonstres--;
			} else {// Sinon obstacle
				obstacles.remove((Obstacle) e);
				nbObstacles--;
			}
		}
	}
}
