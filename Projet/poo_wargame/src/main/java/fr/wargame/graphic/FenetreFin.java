package fr.wargame.graphic;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.wargame.BoucleJeu;
import fr.wargame.IConfig;
import fr.wargame.multimedia.ChargementRessourcesMenu;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Font;

/**
 * Permet d'afficher une fenetre a la fin du jeu
 * permettant au joueur de rejouer ou de retourner au
 * menu.
 */
public class FenetreFin implements IConfig {
	private static JFrame fenetre;
	private static JPanel panneauMenu;
	private static JPanel panneauChargement;
	private static ChargementRessourcesMenu ressourcesMenu;

	private static String[] nomsSauvegardes = new File(".sauv/").list();
	private static int indicePartieInf = 0;
	// On verifie qu'on a pu lire le dossier sinon 0.
	private static int indicePartieSup = (nomsSauvegardes == null) ? 0
			: ((nomsSauvegardes.length >= 5) ? 5 : nomsSauvegardes.length);
	private static int indiceSelectionne = -1;
	private static boolean supprimer = false;

	// Pour l'affichage des sauvegardes et des scores
	private static Image parchemin; // width = 251, height = 377
	private static final Font POLICE_PARCHEMIN = new Font("Brush Script MT", Font.PLAIN, TAILLE_POLICE * 2);

	public static void main(String[] args) throws MidiUnavailableException, IOException, InvalidMidiDataException {
		creerFenetreMenu(1);
	}

	/**
	 * Permet de creer la fenetre de fin de jeu suivant le resultat de la partie.
	 * 
	 * @param i
	 *          <ul>
	 *          <li>1 si le joueur a perdu</li>
	 *          <li>2 si le joueur a gagne</li>
	 *          </ul>
	 **/
	public static void creerFenetreMenu(int i) {
		// On charge les ressources
		ressourcesMenu = new ChargementRessourcesMenu();
		ressourcesMenu.chargement();
		parchemin = ressourcesMenu.parchemin;

		// creation de la fenetre
		fenetre = new JFrame("THE END");
		if (i == 2) {
			ImageIcon win = ressourcesMenu.win;

			fenetre.setSize(win.getIconWidth(), win.getIconHeight());

			// Le Menu du jeu
			panneauMenu = new JPanel() {
				private static final long serialVersionUID = 1L;
				private final Image img = ressourcesMenu.win.getImage();

				@Override
				protected void paintComponent(Graphics g) {

					g.drawImage(img, 0, 0, this);
					super.paintComponent(g);

					g.setFont(POLICE_LEGENDE);
				}
			};
		} else if (i == 1) {
			ImageIcon over = ressourcesMenu.over;

			fenetre.setSize(over.getIconWidth(), over.getIconHeight());

			// Le Menu du jeu
			panneauMenu = new JPanel() {
				private static final long serialVersionUID = 1L;
				private final Image img = ressourcesMenu.over.getImage();

				@Override
				protected void paintComponent(Graphics g) {

					g.drawImage(img, 0, 0, this);
					super.paintComponent(g);

					g.setFont(POLICE_LEGENDE);
				}
			};
		}
		/*
		 * ---
		 * parametrage de la fenetre
		 * ---
		 */
		// fenetre.setResizable(false);
		fenetre.setLocation(100, 100);
		fenetre.setBackground(Color.BLACK);
		fenetre.setVisible(true);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		panneauMenu.setLayout(null);
		/* ----- On cree les boutons ----- */
		int x = 240, y = 106;// On se place en dessous du Titre "Wargame"

		JButton jouer = creerBoutonMenu(x, y, "<html>Nouvelle<br />Partie</html>", Color.green.darker());
		y += jouer.getHeight() + 20;
		JButton charger = creerBoutonMenu(x, y, "<html>Charger<br />Partie</html>", Color.CYAN.darker().darker());
		y += jouer.getHeight() + 20;

		/* ----- On definis les actions des boutons ----- */
		jouer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// On ferme le menu et lance une partie :
				fenetre.dispose();
				new Thread(new BoucleJeu()).start();
			}
		});

		charger.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// On ferme le menu
				// fenetre.dispose();
				// new Thread(new BoucleJeu(".sauv/test.serv")).start();
				fenetre.add(panneauChargement);
				panneauChargement.repaint();

				// On efface les boutons :
				jouer.setVisible(false);
				charger.setVisible(false);
			}
		});

		panneauMenu.setOpaque(false);
		fenetre.setContentPane(panneauMenu);
		fenetre.setVisible(true);

		// Le Menu de chargement :
		panneauChargement = new JPanel() {
			private static final long serialVersionUID = 1L;

			protected void paintComponent(Graphics g) {
				int i, x = 25, y = 106;

				super.paintComponent(g);

				g.drawImage(parchemin, 0, 0, null);
				g.setFont(POLICE_PARCHEMIN);

				for (i = indicePartieInf; i < indicePartieSup; i++) {
					g.setColor(Color.GRAY);
					g.fillOval(x - 10, y - 16, 15, 15);
					g.setColor(COULEUR_TEXTE);
					g.drawString(nomsSauvegardes[i], x + 15, y);

					y += 50;
				}

				// dessine une croix pour quitter :
				g.setColor(Color.red);
				g.drawLine(220, 40, 235, 20);
				g.drawLine(220, 20, 235, 40);

				/*
				 * dessine des fleches :
				 * pour afficher les autre sauvegarde
				 * car que 5 a l'ecran en meme temps.
				 */
				if (indicePartieInf > 0) {
					g.setColor(Color.GREEN);
					g.drawLine(100, 45, 120, 20);// /
					g.drawLine(120, 20, 140, 45);// \
					g.drawLine(140, 45, 100, 45);// _
				}

				if (indicePartieSup < nomsSauvegardes.length) {
					g.drawLine(100, 335, 120, 360);// \
					g.drawLine(120, 360, 140, 335);// /
					g.drawLine(140, 335, 100, 335);// _
				}

			}
		};
		panneauChargement.setOpaque(false);
		panneauChargement.setBounds(230, 72, parchemin.getWidth(null), parchemin.getHeight(null));

		// Pour savoir quelle partie l'utilisateur selectionne
		panneauChargement.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {

				if (e.getY() <= 20 && e.getX() >= 50 && e.getX() <= 100) {
					// Fleche pour remonter l'affichage des sauvegardes :
					if (indicePartieInf >= 5) {
						indicePartieInf -= 5;
						indicePartieSup -= 5;
					}
				} else if (e.getY() >= 280 && e.getX() >= 50 && e.getX() <= 100) {
					// Fleche pour descendre l'affichage des sauvegardes :
					indicePartieSup += 5;
					if (indicePartieSup <= nomsSauvegardes.length) {
						indicePartieInf -= 5;
					} else {
						indicePartieSup = nomsSauvegardes.length;
						indicePartieInf = indicePartieSup - 5;
					}
				}

				else if (e.getX() >= 220 && e.getX() <= 235 && e.getY() >= 20 && e.getY() <= 40) {
					// Appuie sur la croix
					fenetre.remove(panneauChargement);
					// Reactiver les boutons du menu
					jouer.setVisible(true);
					charger.setVisible(true);
					fenetre.repaint();
				} else if (e.getX() >= 25 && e.getX() <= 335) {
					int i, n = 80;
					System.out.println("Click dans l'espace ou sont ecris les nom des sauvegardes " + e.getPoint());
					indiceSelectionne = -1;
					for (i = 0; i < indicePartieSup; i++) {
						System.out.println("i= " + i + " n= " + n + " y= " + e.getY());
						if (e.getY() >= n && e.getY() <= (n + 25)) {
							indiceSelectionne = i;
							break;
						}
						n += 45;
					}

					if (indiceSelectionne != -1 && nomsSauvegardes[indiceSelectionne] != null) {
						System.out.println("Une sauvegarde a ete selectionnee.");
						if (supprimer) {

							// il faut effacer le fichier de sauvegarde et demander confirmation
							if (JOptionPane.showConfirmDialog(
									null,
									"Voulez vous ecraser la sauvegarde '" + nomsSauvegardes[indiceSelectionne] + "' ?",
									"Sauvegarde",
									JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == 0)
								;
							{
								File sauv = new File(nomsSauvegardes[indiceSelectionne]);
								sauv.delete();
								nomsSauvegardes[indiceSelectionne] = null;
								panneauChargement.repaint();
							}
						} else {
							// Il faut lancer la partie :
							fenetre.dispose();
							new Thread(new BoucleJeu(".sauv/" + nomsSauvegardes[indiceSelectionne])).start();
						}
					}
				}
			}
		});
		;
	}

	/**
	 * Cree des JButton au format desire pour le menu du jeu wargame
	 * 
	 * @param x       : coord x NO du bouton
	 * @param y       : coord y NO du bouton
	 * @param label   : String affichee sur le bouton
	 * @param couleur : Color pour la couleur du bouton
	 * @return un JButton
	 */
	private static JButton creerBoutonMenu(int x, int y, String label, Color couleur) {
		JButton bouton = new JButton(label);
		bouton.setFont(POLICE_BOUTON);
		bouton.setForeground(COULEUR_TEXTE);
		bouton.setBackground(couleur);
		bouton.setBounds(x, y, LARGEUR_BOUTON, HAUTEUR_BOUTON);
		bouton.setFocusPainted(false);

		panneauMenu.add(bouton);
		return bouton;
	}
}
