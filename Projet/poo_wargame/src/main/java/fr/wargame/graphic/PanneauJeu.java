package fr.wargame.graphic;

import javax.imageio.ImageIO;
import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.wargame.BoucleJeu;
import fr.wargame.FenetreJeu;
import fr.wargame.IConfig;
import fr.wargame.characters.Heros;
import fr.wargame.map.Carte;
import fr.wargame.map.Element;
import fr.wargame.map.Position;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.awt.Toolkit;
import java.awt.Cursor;
import java.awt.Point;

/**
 * S'occupe de l'affichage du jeu.
 */
public class PanneauJeu extends JPanel implements IConfig {

	private static final long serialVersionUID = 1L;
	private Carte carte;
	private BufferedImage background;
	public static boolean rafraichirStats = true; // pour ne pas les reaficher inutilement

	/* les composants du panneau */
	private JFrame fenetre = new JFrame();
	private JPanel infosTour;

	/* pour les evenements de la souris */
	/** position du Point clique **/
	private Position positionClick = new Position(0, 0);
	/** position courante de la souris selectionne **/
	private Position positionCourante = new Position(0, 0);
	/** position courante du heros selectionne **/
	private Position positionHeros = new Position(0, 0);
	/** le heros selectionne **/
	private Heros heros;
	/* Permet de savoir si un heros est selectionne **/
	private boolean herosSelectionne = false; // permet de savoir si l'image a ete clickee

	/** Pour personnaliser le curseur de la souris **/
	Toolkit tk = Toolkit.getDefaultToolkit();
	/** Curseur qui prend la forme du Heros selectionee **/
	Cursor curseurHeros;
	/** Curseur en forme de main **/
	Cursor curseurMain = new Cursor(Cursor.HAND_CURSOR);

	/* les evenements clavier */
	private Position positionClavier = new Position(0, 0);
	private boolean utiliseClavier = false;

	/**
	 * Creer un JPannel permettant de gerer l'affichage du jeu.
	 * 
	 * @param c : la carte du jeu
	 */
	public PanneauJeu(Carte c) {
		super();
		carte = c;
		creerFenetreJeu();

		this.setLayout(null);
		this.setSize(LARGEUR_CARTE * NB_PIX_CASE, HAUTEUR_CARTE * NB_PIX_CASE);
		this.setBackground(COULEUR_INCONNU);

		// sprite dans element en static;
		background = Element.sprite.carte1;

		// Ajout des ecouteurs d'evenement au panneau
		EvenementSouris souris = new EvenementSouris();
		this.addMouseListener(souris);
		this.addMouseMotionListener(souris);
		this.addKeyListener(raccourcisClavier());

		// reclame le focus pour les event clavier
		this.setFocusable(true);
		this.requestFocus();
	}

	public void chargerImageCarte(String chemin) {
		try {
			background = ImageIO
					.read(/* getClass().getClassLoader().getResource(chemin) */new File("images/" + chemin));
			// TODO: use DIR_SAUVEGARDE
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Cree la fenetre principale du jeu.
	 */
	private void creerFenetreJeu() {
		fenetre = new JFrame("Wargame- Pour gagner tuez tous les monstres.");

		/* ----- Creation d'un bouton pour terminer le tour ----- */
		infosTour = new JPanel() {
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				g.drawString(carte.getStats(),
						10, 20);
			}
		};
		JButton boutonFinTour = new JButton("Fin Tour");
		infosTour.add(boutonFinTour);
		boutonFinTour.setBackground(Color.GREEN);
		boutonFinTour.setSize(20, 20);

		boutonFinTour.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BoucleJeu.tourJ1 = !BoucleJeu.tourJ1;
			}
		});

		/* ----- Creation d'un bouton pour sauvegarder la partie ---- */
		JButton sauvegarde = new JButton("Sauvegarde");
		infosTour.add(sauvegarde);
		sauvegarde.setBackground(Color.BLUE.darker());
		sauvegarde.setSize(20, 20);

		sauvegarde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int tmps;
				int erreur = 0;
				String chemin = JOptionPane.showInputDialog(null, "donnez un nom a votre partie :");

				if (chemin != null) {

					/*
					 * On verifie que l'utilisateur n'a pas donne de chemin.
					 * Si c'est le cas on garde juste le nom en bout de chemin
					 */
					tmps = chemin.lastIndexOf(File.pathSeparator);
					if (tmps != -1) {
						chemin = chemin.substring(tmps);
					}
					/* On rajoute le chemin pour le dossier de sauvegarde + l'extension */
					chemin = DIR_SAUVEGARDE + chemin + EXTENSION_SAUVEGARDE;

					/* On test si le fichier existe, puis sauvegarde la Partie */

					// File fichier = new File(chemin);
					// File fichier = new
					// File(getClass().getClassLoader().getResource(chemin).getFile());
					File fichier = new File(chemin);
					if (fichier.exists()) {
						erreur = JOptionPane.showConfirmDialog(null,
								"Une sauvegarde du meme nom existe, voulez vous l'ecraser ?", "Sauvegarde",
								JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
					}
					if (erreur == 1) {
						return;
					}

					try {
						ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(chemin));
						oos.writeObject(carte);
						oos.close();

					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
						System.err.println("Erreur sauvegarde : file not found - " + chemin);
						erreur++;
					} catch (IOException e1) {
						e1.printStackTrace();
						System.err.println("Erreur sauvegarde : IO exception - " + chemin);
						erreur++;
					} finally {
						JOptionPane.showMessageDialog(
								null,
								"Sauvegarde " + ((erreur == 0) ? "effectue avec succes." : "non effectuee."),
								"Resultat sauvegarde",
								(erreur == 0) ? JOptionPane.INFORMATION_MESSAGE : JOptionPane.ERROR_MESSAGE);
						if (erreur == 0) { // on redirige dans le menu

							if (BoucleJeu.currentThread != null) {
								BoucleJeu.currentThread.interrupt();
							}

							new Thread(new Thread() {
								public void run() {
									try {
										FenetreJeu.main(null);
									} catch (MidiUnavailableException | IOException | InvalidMidiDataException e) {
										e.printStackTrace();
									}
								}
							}).start();
							fenetre.dispose();
						}
					}
				}
			}
		});

		/* ----- Ajout des composants a la fenetre ----- */
		fenetre.getContentPane().setLayout(new BorderLayout());

		fenetre.getContentPane().add(this, BorderLayout.CENTER);
		fenetre.getContentPane().add(infosTour, BorderLayout.NORTH);

		/* ----- Parametrage de la fenetre ----- */
		fenetre.setLocation(POSITION_X, POSITION_Y);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// fenetre.setResizable(false);
		fenetre.setSize((LARGEUR_CARTE + 1) * NB_PIX_CASE, HAUTEUR_CARTE * NB_PIX_CASE + 100);
		fenetre.setVisible(true);
	}

	public JFrame getFenetre() {
		return fenetre;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		// on efface l'affichage precedent
		// g.setColor(COULEUR_INCONNU);
		// g.fillRect(0, 0, LARGEUR_CARTE*NB_PIX_CASE, HAUTEUR_CARTE*NB_PIX_CASE);

		/* ----- Dessin des bordures de la carte ----- */
		/*
		 * g.setColor(COULEUR_TEXTE);
		 * int i, x;
		 * 
		 * for(i=0; i<=LARGEUR_CARTE; i++) {
		 * x = i*NB_PIX_CASE;
		 * // ligne vertical
		 * g.drawLine(x, 0, x, HAUTEUR_CARTE*NB_PIX_CASE);
		 * }
		 * for(i=0; i<=HAUTEUR_CARTE; i++) {
		 * x = i*NB_PIX_CASE;
		 * // ligne horizontale
		 * g.drawLine(0, x, LARGEUR_CARTE*NB_PIX_CASE, x);
		 * }
		 */

		/* ----- Dessin des Elements de la Carte ----- */

		g.drawImage(background, 0, 0, null);
		carte.toutDessiner(g, heros);
		if (herosSelectionne) {
			heros.seDessiner(g, (int) positionHeros.getX(), (int) positionHeros.getY());
		}

		if (utiliseClavier) {
			g.setColor(COULEUR_FORET);
			g.drawRect(positionClavier.getX() * NB_PIX_CASE, positionClavier.getY() * NB_PIX_CASE, NB_PIX_CASE,
					NB_PIX_CASE);
		}

		if (rafraichirStats) {
			infosTour.repaint();
			rafraichirStats = false;
		}
	}

	/* ----- ECOUTEUR D'EVENEMENT DE LA SOURIS ----- */

	private class EvenementSouris extends MouseAdapter {
		final Point pointCurseur = new Point(NB_PIX_CASE / 2, NB_PIX_CASE / 2); // l'endroit de l'image representant le
																				// milieu du curseur

		@Override
		public void mousePressed(MouseEvent e) {

			// transforme le Point en Position de la carte
			positionClick.setY((int) (e.getPoint().getY() / NB_PIX_CASE));
			positionClick.setX((int) (e.getPoint().getX() / NB_PIX_CASE));

			// click sur un heros ?
			herosSelectionne = (positionClick.estValide() && carte.getElement(positionClick) instanceof Heros);
			if (herosSelectionne) {
				heros = (Heros) carte.getElement(positionClick);
				carte.parcoursLargeur(heros); // remplis l'attribut visite pour avoir les cases accessibles du Heros
												// courant
				positionHeros.setPosition(positionClick);

				curseurHeros = tk.createCustomCursor(heros.getAnimation().getImageActuelle(), pointCurseur,
						"curseur heros");
				PanneauJeu.this.setCursor(curseurHeros);

				// change animation
				heros.getAnimation().stop();
				heros.getAnimation().walking();
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			if (herosSelectionne) {
				// effectuer l'action du heros a la position du click
				carte.actionHeros(positionClick, positionHeros);

				// change animation
				heros.animation.stop();
				heros.animation.idle();

				// on deselectionne le heros
				herosSelectionne = false;
				heros = null;
				repaint();

				// remet le curseur en mode normal
				PanneauJeu.this.setCursor(null);
			}
		}

		@Override
		public void mouseDragged(MouseEvent e) {

			if (herosSelectionne) {
				// 1. On transforme le Point en Position
				positionCourante.setY((int) (e.getPoint().getY() / NB_PIX_CASE));
				positionCourante.setX((int) (e.getPoint().getX() / NB_PIX_CASE));

				// 2. On verifie que ce point est a porte de deplacement du heros et vide

				if (positionCourante.estValide()
						&& heros.getPosition().estVoisine(positionCourante, heros.getPortee())
						&& carte.accessible(positionCourante)) {
					// 3. On translate la position graphique du Heros
					positionHeros.setPosition(positionCourante);

					// 4. Actualisation du panneau
					repaint();
				}
			}
		}

		public void mouseMoved(MouseEvent e) {
			Element elem;

			if (utiliseClavier) {
				utiliseClavier = false;
				herosSelectionne = false;
				heros = null;
			}

			// transforme le Point en Position de la carte
			positionClick.setY((int) (e.getPoint().getY() / NB_PIX_CASE));
			positionClick.setX((int) (e.getPoint().getX() / NB_PIX_CASE));

			// Survole -t on un element visible?
			elem = carte.getElement(positionClick);
			if (elem instanceof Element && carte.visible(positionClick)) {
				// on affiche les infos de l'element :
				PanneauJeu.this.setToolTipText(
						"<html><head><style>body { background-color : #558888; } h1 { text-align: center; margin : none; padding-down : 5px;} ul {padding : 0px; margin-left: 10px;} div{text-align:center;}</style></head><body>"
								+ elem.toString()
								+ "</body></html>");
				if (elem instanceof Heros) {

					PanneauJeu.this.setCursor(curseurMain);
				}
			} else {
				PanneauJeu.this.setCursor(null);
			}
		}
	}

	/* ----- RACCOURCIS CLAVIER ----- */

	KeyAdapter raccourcisClavier() {
		return new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				int dx = 0, dy = 0;
				utiliseClavier = true;

				switch (e.getKeyCode()) {
					case KeyEvent.VK_UP:
						dy = -1;
						break;
					case KeyEvent.VK_DOWN:
						dy = 1;
						break;
					case KeyEvent.VK_RIGHT:
						dx = 1;
						break;
					case KeyEvent.VK_LEFT:
						dx = -1;
						break;
					case KeyEvent.VK_ENTER:
						if (herosSelectionne) {
							if (herosSelectionne) {
								carte.actionHeros(positionClavier, positionHeros);

								// change animation
								heros.animation.stop();
								heros.animation.idle();

								// on deselectionne le heros
								herosSelectionne = false;
								heros = null;
								repaint();
							}
						} else {
							herosSelectionne = (/* positionClavier.estValide() && */ carte
									.getElement(positionClavier) instanceof Heros);
							if (herosSelectionne) {
								heros = (Heros) carte.getElement(positionClavier);
								carte.parcoursLargeur(heros); // remplis l'attribut visite pour avoir les cases
																// accessibles du Heros courant
								positionHeros.setPosition(positionClavier);

								// change animation
								heros.getAnimation().stop();
								heros.getAnimation().walking();
							}
						}
						return; // sort de la fct
					default:
						return; // sort de la fct
				}

				if (herosSelectionne) { // maj Position du heros ?
					positionHeros.setX(positionHeros.getX() + dx);
					positionHeros.setY(positionHeros.getY() + dy);

					if (positionHeros.estValide()
							&& heros.getPosition().estVoisine(positionHeros, heros.getPortee())
							&& carte.accessible(positionHeros)) {

						// Actualisation du panneau
						repaint();
					} else { // position pas valide on revient en arriere
						positionHeros.setX(positionHeros.getX() - dx);
						positionHeros.setY(positionHeros.getY() - dy);
					}
				} else { // on avance la position clavier
					positionClavier.setX(positionClavier.getX() + dx);
					positionClavier.setY(positionClavier.getY() + dy);

					if (!positionClavier.estValide()) {
						positionClavier.setX(positionClavier.getX() - dx);
						positionClavier.setY(positionClavier.getY() - dy);
					} else {
						repaint();
					}
				}
			}

		};
	}

}
