package fr.wargame;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import fr.wargame.characters.Heros;
import fr.wargame.characters.Monstres;
import fr.wargame.characters.Soldat;
import fr.wargame.map.Carte;
import fr.wargame.map.Element;
import fr.wargame.map.Obstacle;
import fr.wargame.map.Position;

public class Test implements IConfig {

	public static void main(String[] args) {
		Map<Position, Element> carte = new HashMap<Position, Element>(IConfig.NB_ELEMENTS);
		;
		LinkedList<Heros> heros = new LinkedList<Heros>();
		LinkedList<Monstres> monstres = new LinkedList<Monstres>();

		// On place les elements sur la carte :
		int i, x = 1, y = HAUTEUR_CARTE - 1;
		Position tempP;
		Heros tempH;
		Monstres tempM;
		Obstacle tempO;

		for (i = 0; i < NB_HEROS; i++) {

			tempP = new Position(y, x);
			y -= 1;

			tempH = new Heros(null, Soldat.TypesH.getTypeHAlea(), (char) ('A' + i), tempP);

			// ajoute dans la liste et la Carte
			heros.add(tempH);
			carte.put(tempP, tempH);
		}

		// 2. creation des Monstres
		x = 9;
		y = HAUTEUR_CARTE - 1;
		for (i = 0; i < NB_MONSTRES; i++) {
			// on lui donne une position aleatoire

			tempP = new Position(y, x);
			x += 1;
			tempM = new Monstres(null, Soldat.TypesM.getTypeMAlea(), i, tempP);

			// ajout dans la liste et Carte
			monstres.add(tempM);
			carte.put(tempP, tempM);
		}

		// 3. creation et placement des Obstacles
		tempP = new Position(14, 3);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(14, 4);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		// tempP = new Position(13, 3);
		// tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		// carte.put(tempP, tempO);

		tempP = new Position(13, 4);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(13, 5);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(12, 4);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(12, 5);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(11, 4);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(11, 5);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(11, 6);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(8, 6);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(8, 7);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(8, 8);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(8, 9);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(7, 8);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(7, 9);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		tempP = new Position(7, 10);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
		carte.put(tempP, tempO);

		for (i = 9; i <= 18; i++) {
			tempP = new Position(6, i);
			tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
			carte.put(tempP, tempO);

			if (i >= 10) {
				tempP = new Position(5, i);
				tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
				carte.put(tempP, tempO);
			}
			if (i >= 12) {
				tempP = new Position(4, i);
				tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
				carte.put(tempP, tempO);
			}
		}

		for (i = 22; i < LARGEUR_CARTE; i++) {
			tempP = new Position(6, i);
			tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
			carte.put(tempP, tempO);

			tempP = new Position(5, i);
			tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
			carte.put(tempP, tempO);

			tempP = new Position(4, i);
			tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);
			carte.put(tempP, tempO);
		}

		BitSet[] elementsVisibles = new BitSet[HAUTEUR_CARTE];
		for (i = 0; i < HAUTEUR_CARTE; i++) {
			elementsVisibles[i] = new BitSet(LARGEUR_CARTE);
		}

		Carte c = new Carte(carte, heros, monstres, elementsVisibles);

		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("carte_1.ser"));
			oos.writeObject(c);
			oos.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			System.err.println("Erreur sauvegarde : file not found");
		} catch (IOException e1) {
			e1.printStackTrace();
			System.err.println("Erreur sauvegarde : IO exception");
		}

		tempP = new Position(3, 13);
		tempO = new Obstacle(Obstacle.TypeObstacle.EAU, tempP);

		System.out.println("Carte enregistree");
	}
}
