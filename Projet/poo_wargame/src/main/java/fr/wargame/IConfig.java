package fr.wargame;

import java.awt.Color;
import java.awt.Font;

/** Contient les parametres du jeu **/
public interface IConfig {
	int LARGEUR_CARTE = 25;
	int HAUTEUR_CARTE = 15; // en nombre de cases
	int NB_PIX_CASE = 32;
	int POSITION_X = 100;
	int POSITION_Y = 50; // Position de la fenetre
	int NB_HEROS = 6;
	int NB_MONSTRES = 15;
	int NB_OBSTACLES = 50;
	int NB_ELEMENTS = NB_HEROS + NB_MONSTRES + NB_OBSTACLES;
	int PORTEE_MAX = 8;
	Color COULEUR_VIDE = Color.white, COULEUR_INCONNU = Color.lightGray;
	Color COULEUR_TEXTE = Color.black, COULEUR_MONSTRES = Color.black;
	Color COULEUR_HEROS = Color.red.darker(), COULEUR_HEROS_DEJA_JOUE = Color.pink;
	Color COULEUR_EAU = Color.blue, COULEUR_FORET = Color.green, COULEUR_ROCHER = Color.gray;
	Color COULEUR_CHEMIN = new Color(14, 6, 0);
	Color COULEUR_FOG = Color.black;

	int LARGEUR_BOUTON = 225, HAUTEUR_BOUTON = 60, ARRONDIS_BOUTON = 15;
	int TAILLE_POLICE = 26;
	Font POLICE_BOUTON = new Font("Cooper Black", Font.PLAIN, TAILLE_POLICE);
	Font POLICE_LEGENDE = new Font("Castellar", Font.PLAIN, TAILLE_POLICE);

	String DIR_SAUVEGARDE = "/files/.sauv/";
	String EXTENSION_SAUVEGARDE = ".ser";
}
