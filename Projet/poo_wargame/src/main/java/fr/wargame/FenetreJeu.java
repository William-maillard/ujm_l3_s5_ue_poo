package fr.wargame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import fr.wargame.graphic.PanneauCreation;
import fr.wargame.multimedia.ChargementRessourcesMenu;

import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 * Permet de creer la JFrame du jeu contenant des boutons pour :
 * <ul>
 * <li>Lancer une partie</li>
 * <li>Charger une partie</li>
 * <li>Creer une carte personalisee</li>
 * <li>Quitter</li>
 * </ul>
 *
 */
public class FenetreJeu implements IConfig {
	private static JFrame fenetre;
	private static JPanel panneauMenu;
	private static JPanel panneauChargement;
	private static ChargementRessourcesMenu ressourcesMenu;

	public static String[] nomsSauvegardes;
	private static int indicePartieInf = 0;
	private static int indicePartieSup;
	private static int indiceSelectionne = -1;
	private static boolean supprimer = false;

	// Pour l'affichage des sauvegardes et des scores
	private static Image parchemin; // width = 251, height = 377
	private static final Font POLICE_PARCHEMIN = new Font("Brush Script MT", Font.PLAIN, TAILLE_POLICE * 2);

	/** Lance le menu du jeu. **/
	public static void main(String[] args) throws MidiUnavailableException, IOException, InvalidMidiDataException {

		// On charge les ressources
		ressourcesMenu = new ChargementRessourcesMenu();
		ressourcesMenu.chargement();
		// nomsSauvegardes = ressourcesMenu.creerListeSauvegarde();
		if (nomsSauvegardes != null) {
			indicePartieSup = nomsSauvegardes.length;
		}
		if (indicePartieSup > 5)
			indicePartieSup = 5;

		parchemin = ressourcesMenu.parchemin;

		creerFenetreMenu();
		// new Thread(new BoucleJeu()).start();

	}

	/** Cree la JFrame du menu. **/
	public static void creerFenetreMenu() {

		// creation de la fenetre
		fenetre = new JFrame("Wargame");
		ImageIcon menu = ressourcesMenu.menu;

		/*
		 * ---
		 * parametrage de la fenetre
		 * ---
		 */
		fenetre.setSize(menu.getIconWidth(), menu.getIconHeight());
		/*
		 * -- On supprime la barre de menu par defaut
		 * (du coups on a un bouton quitter pour fermer la fenetre) --
		 */
		fenetre.setUndecorated(true);

		/* -- On centre la fenetre a l'ecran -- */
		Dimension tailleEcran = Toolkit.getDefaultToolkit().getScreenSize();
		fenetre.setLocation((int) (tailleEcran.getWidth() / 2 - fenetre.getWidth() / 2),
				(int) (tailleEcran.getHeight() / 2 - fenetre.getHeight() / 2));

		// Le Menu du jeu
		panneauMenu = new JPanel() {
			private static final long serialVersionUID = 1L;
			private final Image img = ressourcesMenu.menu.getImage();

			@Override
			public void paintComponent(Graphics g) {
				int x, y;

				g.drawImage(img, 0, 0, this);
				super.paintComponent(g);

				g.setFont(POLICE_LEGENDE);

				/* Les Heros */
				g.setColor(Color.white.darker());
				g.fillRect(40, 30, 115, 30);
				g.setColor(COULEUR_TEXTE);
				g.drawString("Heros", 50, 55);

				x = 70;
				y = 80;
				g.drawImage(ressourcesMenu.alchemist, x, y, null);
				y += 40;
				g.drawImage(ressourcesMenu.butcher, x, y, null);
				y += 40;
				g.drawImage(ressourcesMenu.archer, x, y, null);
				y += 40;
				g.drawImage(ressourcesMenu.thief, x, y, null);

				/* Les Monstres */
				g.setColor(Color.red.darker().darker());
				g.fillRect(530, 30, 170, 30);
				g.setColor(COULEUR_TEXTE);
				g.drawString("Monstres", 535, 55);

				x = 600;
				y = 80;
				g.drawImage(ressourcesMenu.largeKnight, x, y, null);
				y += 40;
				g.drawImage(ressourcesMenu.heavyKnight, x, y, null);
				y += 40;
				g.drawImage(ressourcesMenu.eliteKnight, x, y, null);
			}
		};

		panneauMenu.setLayout(null);
		/* ----- On cree les boutons ----- */
		int x = 240, y = 106;// On se place en dessous du Titre "Wargame"

		JButton jouer = creerBoutonMenu(x, y, "<html>Nouvelle<br />Partie</html>", Color.green.darker());
		y += jouer.getHeight() + 20;
		JButton charger = creerBoutonMenu(x, y, "<html>Charger<br />Partie</html>", Color.CYAN.darker().darker());
		y += jouer.getHeight() + 20;
		JButton creation = creerBoutonMenu(x, y, "Creation", Color.GRAY);
		y += creation.getHeight() + 20;
		JButton quitter = creerBoutonMenu(x, y, "Quitter", Color.RED);
		// JButton scores = creerBoutonMenu(x, y, "Meilleurs scores", Color.GRAY);

		/* ----- On definis les actions des boutons ----- */
		jouer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// On ferme le menu et lance une partie :
				fenetre.dispose();
				new Thread(new BoucleJeu()).start();
			}
		});

		charger.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// On ferme le menu
				// fenetre.dispose();
				// new Thread(new BoucleJeu(".sauv/test.serv")).start();
				FenetreJeu.nomsSauvegardes = new File(".sauv").list();
				fenetre.add(panneauChargement);
				panneauChargement.repaint();

				// On efface les boutons :
				jouer.setVisible(false);
				charger.setVisible(false);
				creation.setVisible(false);
				quitter.setVisible(false);
			}
		});

		creation.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				new PanneauCreation(ressourcesMenu);
				fenetre.setVisible(false);
			}
		});

		quitter.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				fenetre.dispose();
				System.exit(0);
			}
		});

		panneauMenu.setOpaque(false);
		fenetre.setContentPane(panneauMenu);
		fenetre.setVisible(true);

		/* Le Menu de chargement : */

		panneauChargement = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				int i, x = 25, y = 106;

				super.paintComponent(g);

				g.drawImage(parchemin, 0, 0, null);
				g.setFont(POLICE_PARCHEMIN);

				for (i = indicePartieInf; i < indicePartieSup; i++) {
					g.setColor(Color.GRAY);
					g.fillOval(x - 10, y - 16, 15, 15);
					g.setColor(COULEUR_TEXTE);
					g.drawString(nomsSauvegardes[i].substring(0, nomsSauvegardes[i].length() - 4), x + 15, y);

					y += 50;
				}

				// dessine une croix pour quitter :
				g.setColor(Color.red);
				g.drawLine(220, 40, 235, 20);
				g.drawLine(220, 20, 235, 40);

				/*
				 * dessine des fleches :
				 * pour afficher les autre sauvegarde
				 * car que 5 a l'ecran en meme temps.
				 */
				if (indicePartieInf > 0) {
					g.setColor(Color.GREEN);
					g.drawLine(100, 45, 120, 20);// /
					g.drawLine(120, 20, 140, 45);// \
					g.drawLine(140, 45, 100, 45);// _
				}

				if (indicePartieSup < nomsSauvegardes.length) {
					g.drawLine(100, 335, 120, 360);// \
					g.drawLine(120, 360, 140, 335);// /
					g.drawLine(140, 335, 100, 335);// _
				}
			}

		};

		panneauChargement.setOpaque(false);
		panneauChargement.setBounds(230, 72, parchemin.getWidth(null), parchemin.getHeight(null));

		/* Pour savoir quelle partie l'utilisateur selectionne */
		panneauChargement.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {

				if (e.getY() <= 45 && e.getX() >= 100 && e.getX() <= 140) {
					// Fleche pour remonter l'affichage des sauvegardes :
					if (indicePartieInf >= 5) {
						indicePartieInf -= 5;
						indicePartieSup -= 5;
					} else {
						indicePartieInf = 0;
						indicePartieSup = 5;
					}
					System.out.println("remonte : " + indicePartieInf + "-" + indicePartieSup);
					panneauChargement.repaint();
				} else if (e.getY() >= 335 && e.getX() >= 100 && e.getX() <= 140) {
					// Fleche pour descendre l'affichage des sauvegardes :
					indicePartieSup += 5;
					if (indicePartieSup <= nomsSauvegardes.length) {
						indicePartieInf -= 5;
					} else {
						indicePartieSup = nomsSauvegardes.length;
						indicePartieInf = indicePartieSup - 5;
					}
					System.out.println("descend : " + indicePartieInf + "-" + indicePartieSup);
					panneauChargement.repaint();
				}

				else if (e.getX() >= 220 && e.getX() <= 235 && e.getY() >= 20 && e.getY() <= 40) {
					// Appuie sur la croix
					fenetre.remove(panneauChargement);
					// Reactiver les boutons du menu
					jouer.setVisible(true);
					charger.setVisible(true);
					creation.setVisible(true);
					quitter.setVisible(true);
					fenetre.repaint();
				} else if (e.getX() >= 25 && e.getX() <= 335) {
					int i, n = 80;
					System.out.println("Click dans l'espace ou sont ecris les nom des sauvegardes " + e.getPoint());
					indiceSelectionne = -1;
					for (i = 0; i < indicePartieSup; i++) {
						System.out.println("i= " + i + " n= " + n + " y= " + e.getY());
						if (e.getY() >= n && e.getY() <= (n + 25)) {
							indiceSelectionne = i;
							break;
						}
						n += 50;
					}

					if (indiceSelectionne != -1 && nomsSauvegardes[indiceSelectionne] != null) {
						System.out.println("Une sauvegarde a ete selectionnee.");
						indiceSelectionne += indicePartieInf;
						if (supprimer) {

							// il faut effacer le fichier de sauvegarde et demander confirmation
							if (JOptionPane.showConfirmDialog(
									null,
									"Voulez vous ecraser la sauvegarde '" + nomsSauvegardes[indiceSelectionne] + "' ?",
									"Sauvegarde",
									JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == 0)
								;
							{
								File sauv = new File(getClass().getClassLoader()
										.getResource(nomsSauvegardes[indiceSelectionne]).getFile());
								sauv.delete();
								nomsSauvegardes[indiceSelectionne] = null;
								panneauChargement.repaint();
							}
						} else {
							// Il faut lancer la partie :
							fenetre.dispose();
							new Thread(new BoucleJeu(/* ".sauv/"+ */nomsSauvegardes[indiceSelectionne])).start();
						}
					}
				}
			}
		});
	}

	/**
	 * Cree des JButton au format desire pour le menu du jeu wargame
	 * 
	 * @param x       : coord x NO du bouton
	 * @param y       : coord y NO du bouton
	 * @param label   : String affichee sur le bouton
	 * @param couleur : Color pour la couleur du bouton
	 * @return un JButton
	 */
	private static JButton creerBoutonMenu(int x, int y, String label, Color couleur) {
		JButton bouton = new JButton(label);
		bouton.setFont(POLICE_BOUTON);
		bouton.setForeground(COULEUR_TEXTE);
		bouton.setBackground(couleur);
		bouton.setBounds(x, y, LARGEUR_BOUTON, HAUTEUR_BOUTON);
		bouton.setFocusPainted(false);

		panneauMenu.add(bouton);
		return bouton;
	}

}
