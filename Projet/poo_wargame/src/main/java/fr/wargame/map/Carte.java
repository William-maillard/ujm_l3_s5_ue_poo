package fr.wargame.map;

import java.awt.Graphics;
import java.io.Serializable;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import fr.wargame.IConfig;
import fr.wargame.characters.GeneralMonstre;
import fr.wargame.characters.Heros;
import fr.wargame.characters.ISoldat;
import fr.wargame.characters.Monstres;
import fr.wargame.characters.Soldat;
import fr.wargame.graphic.FenetreFin;
import fr.wargame.graphic.PanneauJeu;

/**
 * Cette classe represente la carte du jeu Wargame et implemente
 * les interactions entre les differents elements qui la composent
 * (soldats, obstacles).
 * 
 * @see IConfig pour les parametres de la carte et du jeu
 */
public class Carte implements ICarte, IConfig, Serializable {

	private static final long serialVersionUID = -4738605375682904022L;
	private Map<Position, Element> carte;
	private LinkedList<Heros> heros;
	private LinkedList<Monstres> monstres;

	/** Joueur ordinateur, il n'est pas serialize avec la carte **/
	public transient GeneralMonstre generalMonstre;

	/** Permet de gerer l'affichage du brouillard de guerre **/
	private BitSet[] elementsVisibles;

	/** Pour calculer et stocker les cases accesibles a un heros : **/
	private int[][] visite = new int[2 * ISoldat.PORTEE_VISUELLE_MAX + 2][2 * ISoldat.PORTEE_VISUELLE_MAX + 2];
	/**
	 * decallage en x et y pour faire la correspondance
	 * entre coord carte et coord tableau visite
	 **/
	private Position origine = new Position(0, 0);//

	/**
	 * Initialise les listes de heros, monstres.
	 * Creer une carte avec les heros a gauche les monstres a droite
	 * et des obstacles.
	 */
	Carte() {
		Position tempP;
		Element tempE;
		int i;

		/* -- On initialise les attributs de Carte -- */
		carte = new HashMap<Position, Element>(NB_ELEMENTS);
		heros = new LinkedList<Heros>();
		monstres = new LinkedList<Monstres>();

		/* -- On cree et place les Elements de la Carte -- */

		// 1. creation des Heros :
		for (i = 0; i < NB_HEROS; i++) {
			// on lui donne une position aleatoire dans la partie gauche de la carte
			tempP = new Position(
					(int) (Math.random() * HAUTEUR_CARTE),
					(int) (Math.random() * (LARGEUR_CARTE / 2 - 1)));
			tempE = new Heros(this, Soldat.TypesH.getTypeHAlea(), (char) ('A' + i), tempP);

			// ajoute dans la liste et la Carte
			heros.add((Heros) tempE);
			carte.put(tempP, tempE);

		}

		// 2. creation des Monstres
		for (i = 0; i < NB_MONSTRES; i++) {
			// on lui donne une position aleatoire
			tempP = new Position(
					(int) (Math.random() * HAUTEUR_CARTE),
					LARGEUR_CARTE / 2 + (int) (Math.random() * (LARGEUR_CARTE / 2) + 1));
			tempE = new Monstres(this, Soldat.TypesM.getTypeMAlea(), i + 1, tempP);

			// ajout dans la liste et Carte
			monstres.add((Monstres) tempE);
			carte.put(tempP, tempE);
		}

		// 3. creation et placement des Obstacles
		for (i = 0; i < NB_OBSTACLES; i++) {
			// cherche une position vide dans la carte
			tempP = this.trouvePositionVide();
			tempE = new Obstacle(Obstacle.TypeObstacle.getObstacleAlea(), tempP);

			// ajout dans la carte
			carte.put(tempP, tempE);
		}

		// init fog
		elementsVisibles = new BitSet[HAUTEUR_CARTE];
		for (i = 0; i < HAUTEUR_CARTE; i++) {
			elementsVisibles[i] = new BitSet(LARGEUR_CARTE);
		}
		fogUpdate();

	}

	/**
	 * Cree une Carte dans le but de la serializer afin
	 * de pouvoir l'utiliser plus tard.
	 * 
	 * @param carte    : une map contenant les elements de la carte
	 * @param heros    : une LinkedList contenant les Heros
	 * @param monstres : une LinkedList contenant les Monstres
	 * @param elem     : un BitSet[] pour le brouillard de guerre
	 */
	public Carte(Map<Position, Element> carte, LinkedList<Heros> heros, LinkedList<Monstres> monstres, BitSet[] elem) {
		this.carte = carte;
		this.heros = heros;
		this.monstres = monstres;
		this.elementsVisibles = elem;

	}

	/**
	 * Permet d'affecter les animations aux soldats
	 * apres le chargement d'une partie.
	 */
	public void loadAnimation() {
		for (Heros h : heros) {
			h.loadAnimation();
		}
		for (Monstres m : monstres) {
			m.loadAnimation();
		}
		// Creation du jouer ordi :
		generalMonstre = new GeneralMonstre(monstres, this);
		// calcul du brouillard de guerre
		fogUpdate();
	}

	@Override
	public Element getElement(Position pos) {
		return carte.get(pos);
	}

	/**
	 * Pour obtenir le nombre restant de Heros et de Monstres
	 * sous forme de String
	 * 
	 * @return un chaine contenant des infos sur la partie en cours.
	 */
	public String getStats() {
		return "Il reste " + heros.size() + " heros et " + monstres.size() + " monstres.";
	}

	@Override
	public Position trouvePositionVide() { // trouve pos vide sur carte sinon null
		Position pos = new Position(0, 0);

		while (true) {
			pos.setY((int) (Math.random() * HAUTEUR_CARTE));
			pos.setX((int) (Math.random() * LARGEUR_CARTE));

			if (!carte.containsKey(pos)) {
				return pos;
			}
		}
	}

	@Override
	public Position trouvePositionVide(Position pos) { // trouve pos vide autours de arg sinon null //afinir
		int x, y, i, j;
		boolean flag = false;
		Position newPos;
		x = pos.getX();
		y = pos.getY();
		for (int a = -1; a < 2; a++) { // check si il y'a des position vide autours de pos sinon trouvePositionVide
										// return null
			for (int b = -1; b < 2; b++) {
				newPos = new Position((y + a), (x + b));
				if (newPos.estValide() && this.getElement(newPos) == null) {
					flag = true; // il y'a au moins une position vide autours de pos, nous pouvons donc
									// continuer.
				}
			}
		}
		if (flag == false) {
			return null; // pas de position vide autours de pos
		}
		while (true) { // selection de la position vide aleatoire ici
			i = (int) (Math.random() * 3) - 1;
			j = (int) (Math.random() * 3) - 1;
			i += y;
			j += x;
			newPos = new Position(i, j);
			if (newPos.estValide()) {
				if (this.getElement(newPos) == null) {
					return newPos;
				}
			}
		}
	}

	@Override
	public Heros trouveHeros() {
		if (heros.size() != 0) {
			return heros.get(((int) Math.random() * heros.size()));
		}
		return null;
	}

	@Override
	public Heros trouveHeros(Position pos) {
		int x = pos.getX(), y = pos.getY();
		Position tempP;

		// on regarde les cases autour de pos
		for (int i = (y - 1); i <= (y + 1); i++) {
			for (int j = (x - 1); j <= (x + 1); j++) {
				tempP = new Position(i, j);
				if (tempP.estValide() && getElement(tempP) instanceof Heros)
					return (Heros) getElement(tempP);
			}
		}
		return null;
	}

	/**
	 * Permet de trouver un monstre dans les positions adjacentes a pos
	 * 
	 * @deprecated
	 **/
	public Monstres trouveMonstres(Position pos) {
		int x, y;
		x = pos.getX();
		y = pos.getY();
		Position tempP;
		for (int i = (y - 1); i <= (y + 1); i++) {
			for (int j = (x - 1); j <= (x + 1); j++) {
				tempP = new Position(i, j);
				if (tempP.estValide() && getElement(tempP) instanceof Monstres)
					return (Monstres) getElement(tempP);
			}
		}
		return null;
	}

	@Override
	public boolean deplaceSoldat(Position pos, Soldat soldat) { // met a jour soldat sur la carte

		if (soldat.getTour() > -1) {
			Position p = soldat.getPosition();

			if (pos.estValide() && getElement(pos) == null) {
				/* on supprime l'entree du soldat */
				carte.remove(p);

				/* on insere le soldat avec sa nouvelle cle */
				p.setPosition(pos);
				carte.put(p, soldat);

				/* on met a jour le brouillard de guerre */
				if (soldat instanceof Heros) {
					fogUpdate();
				}
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean actionHeros(Position pos, Position pos2) { // deplacement attaque
		if (pos2.estValide() && getElement(pos) instanceof Heros) {
			Heros h = (Heros) getElement(pos);
			if (h.getTour() > -1) {

				if (getElement(pos2) instanceof Monstres) {
					Monstres m = (Monstres) getElement(pos2);
					h.combat(m);
					// resultat du combat :
					if (h.estMort()) {
						mort(h);
					} else if (m.estMort()) {
						mort(m);
					}
					return true;
				} else if (!pos.equals(pos2)) {

					if (deplaceSoldat(pos2, h)) {
						h.seDeplace(pos2);
						return true;
					}
				}
			}
		}
		return false;
	}

	public boolean actionMonstre(Position pos, Position pos2) { // deplacement attaque
		if (pos2.estValide() && getElement(pos) instanceof Monstres) {
			Monstres h = (Monstres) getElement(pos);
			System.out.println("Tour " + h.getTour());
			if (h.getTour() > -1) {

				if (getElement(pos2) instanceof Heros) {
					Heros m = (Heros) getElement(pos2);
					h.combat(m);
					// resultat du combat :
					if (h.estMort()) {
						mort(h);
					} else if (m.estMort()) {
						mort(m);
					}
					return true;
				} else if (!pos.equals(pos2)) {

					if (deplaceSoldat(pos2, h)) {
						h.seDeplace(pos2);
						return true;
					}
				}
			}
		}
		return false;
	}

	/*
	 * public void debutTour(int i) { //0 pour hero 1 pour monstres
	 * if(i == 0) {
	 * for(Heros h: heros) {
	 * h.setTour(-1);
	 * }
	 * }
	 * else if(i == 1) {
	 * for(Monstres m: monstres) {
	 * m.setTour(-1);
	 * }
	 * }
	 * }
	 */

	/**
	 * Utiliser a la fin du tour du joueur pour
	 * que les Heros non joues regagne de la vie,
	 * et reinitialise le champs tour des autres.
	 */
	public void finTourHeros() {
		for (Heros h : heros) {

			// si le Heros a pas joue recupere entre [1, tour] pv
			if (h.getTour() != -1) {
				h.setTour(h.getTour() + 1);
				h.setPoints(h.getPoints() + (int) (Math.random() * h.getTour()) + 1);

				// il ne doit pas depasser les pv max
				if (h.getPoints() > h.getType().getPoints())
					h.setPoints(h.getType().getPoints());
			} else {
				// On remet le Heros a non joue pour le prochain tour
				h.setTour(0);
			}
		}
	}

	/**
	 * Utiliser a la fin du tour de l'ordi pour
	 * que les Monstres non joues regagne de la vie,
	 * et reinitialise le champs tour des autres.
	 */
	public void finTourMonstres() {
		for (Monstres m : monstres) {

			// si le Monstre a pas joue recupere entre [1, tour] pv
			if (m.getTour() != -1) {
				m.setTour(m.getTour() + 1);
				m.setPoints(m.getPoints() + (int) (Math.random() * m.getTour()) + 1);

				// il ne doit pas depasser les pv max
				if (m.getPoints() > m.getType().getPoints())
					m.setPoints(m.getType().getPoints());
			} else {
				// On remet le Monstre a non joue pour le prochain tour
				m.setTour(0);
			}
		}
	}

	@Override
	public void mort(Soldat perso) {

		// supprime de la carte
		carte.remove(perso.getPosition());
		// supprime de la liste
		if (perso instanceof Heros) {
			heros.remove(perso);
			fogUpdate();// mise a jour du brouillard de guerre
		} else {
			monstres.remove(perso);
		}
		// actualise l'affichage :
		PanneauJeu.rafraichirStats = true;
	}

	/**
	 * Permet a chaque fin de tour de verifier si
	 * l'un des deux camps a gange.
	 * 
	 * @return
	 *         <ul>
	 *         <li>0 : la partie n'est pas finie.</li>
	 *         <li>1 : les Monstres gagne.</li>
	 *         <li>2 : les Heros gagne.</li>
	 *         </ul>
	 */
	public int fin() {
		if (heros.isEmpty()) {
			FenetreFin.creerFenetreMenu(1);
			return 1;
		} else if (monstres.isEmpty()) {
			FenetreFin.creerFenetreMenu(2);
			return 2;
		}
		return 0;
	}

	/**
	 * utiliser lors du deplacement d'un hero pour mettre a jour le brouillard de
	 * guerre
	 */
	private void fogUpdate() {
		int x, y, porteVisu;
		int i, j;
		Position tempP;

		// reinit a faux les bitSet
		for (i = 0; i < HAUTEUR_CARTE; i++) {
			elementsVisibles[i].clear();
		}

		// on met du noir sur tout ce qui n'est pas a portee visuelle des heros
		for (Heros h : heros) {
			tempP = new Position(0, 0);
			tempP.setPosition(h.getPosition());
			x = tempP.getX();
			y = tempP.getY();
			porteVisu = h.getPortee();

			/*
			 * On passe a true toutes les cases visibles par
			 * les heros
			 */
			for (i = (-porteVisu); i <= porteVisu; i++) {
				tempP.setY(y + i);
				for (j = (-porteVisu); j <= porteVisu; j++) {
					tempP.setX(x + j);
					if (tempP.estValide()) {
						elementsVisibles[tempP.getY()].set(tempP.getX(), true);
					}
				}
			}
		}
	}

	/**
	 * Permet de savoir si la case a la position p est visible
	 * par les Heros.
	 * 
	 * @param p une Position
	 * @return
	 *         <ul>
	 *         <li>true si la position est visible</li>
	 *         <li>false sinon</li>
	 *         </ul>
	 */
	public boolean visible(Position p) {
		return elementsVisibles[p.getY()].get(p.getX());
	}

	// test
	@Override
	public String toString() {
		Position p = new Position(0, 0);
		Element e;
		String s = "";

		for (int i = 0; i < LARGEUR_CARTE; i++) {
			for (int j = 0; j < HAUTEUR_CARTE; j++) {
				p.setX(i);
				p.setY(j);
				e = getElement(p);
				if (e instanceof Element) {
					s += e.toString();
				} else {
					s += " . ";
				}
			}
			s += "\n";
		}
		return s;
	}

	@Override
	// prend un hero en parametre pour ne plus dessiner le hero que l'on drag'n drop
	// a sa place d'origine
	public void toutDessiner(Graphics g, Heros h) {
		// fogUpdate();
		for (Heros hero : heros) {
			if (hero != h) {
				hero.seDessiner(g);
				if (hero.getTour() == -1) {
					g.setColor(COULEUR_HEROS_DEJA_JOUE);
					g.drawRect(hero.getPosition().getX() * NB_PIX_CASE, hero.getPosition().getY() * NB_PIX_CASE,
							NB_PIX_CASE, NB_PIX_CASE);
				}
			}
		}
		for (Monstres monstre : monstres) {
			monstre.seDessiner(g);
		}

		/* Dessin du brouillard */
		int i, j;
		Position tempP = new Position();

		for (i = 0; i < HAUTEUR_CARTE; i++) {
			tempP.setY(i);
			for (j = 0; j < LARGEUR_CARTE; j++) {
				tempP.setX(j);

				/* Si l'element n'est pas visible, on le masque */
				if (!elementsVisibles[tempP.getY()].get(tempP.getX())) {
					g.setColor(COULEUR_FOG);
					g.fillRect(j * NB_PIX_CASE, i * NB_PIX_CASE, NB_PIX_CASE, NB_PIX_CASE);
				}
			}
		}

		/* -- Dessin des cases accessibles -- */
		if (h instanceof Heros) {
			int n = h.getPortee() * 2 + 1, m = n;
			tempP.setX(h.getPosition().getX() - h.getPortee());
			tempP.setY(h.getPosition().getY() - h.getPortee());
			if (tempP.getX() < 0)
				n += tempP.getX();
			if (tempP.getY() < 0)
				m += tempP.getY();

			for (i = 0; i <= n; i++) {
				for (j = 0; j <= m; j++) {
					if (visite[j][i] != -1) {
						g.setColor(COULEUR_HEROS);
						g.drawRect((i + origine.getX()) * NB_PIX_CASE, (j + origine.getY()) * NB_PIX_CASE, NB_PIX_CASE,
								NB_PIX_CASE);

					}
				}
			}
		}
	}

	/* ----- Fonctions pour verifier la validite d'une action ----- */

	/**
	 * Verifie si la trajectoire d'un tir est degagee.<br />
	 * NB : Les tirs ont tous une trajectoire rectiligne.
	 * 
	 * @param depart  : Position
	 * @param arrivee : Position
	 * @return
	 *         <ul>
	 *         <li>true si le tir est possible</li>
	 *         <li>false sinon</li>
	 *         </ul>
	 */
	public boolean trajectoireTir(Position depart, Position arrivee) {
		int dx, dy; /*
					 * permet de savoir si on increment ou decrement la coordonnee
					 * pour aller de la position de depart a celle d'arrivee
					 */
		int x0, y0, x, y;
		Position tempP = new Position();

		/*
		 * 1. On determine le sens du tir :
		 * - horizontal
		 * - vertical
		 * - diagonal
		 */
		/* AXE DES ABSCYSSES */
		dx = arrivee.getX() - depart.getX();
		if (dx < 0) { // La case d'arrivee est a gauche par rapport a celle de depart
			dx = -1;
		} else if (dx > 0) { // La case d'arrivee est a droite par rapport a celle de depart
			dx = 1;
		} else { // La case d'arrivee est aligne horizontalement avec celle de depart
			dx = 0;
		}

		/* AXE DES ORDONNEES */
		dy = arrivee.getY() - depart.getY();
		if (dy < 0) { // La case d'arrivee est en bas par rapport a celle de depart
			dy = -1;
		} else if (dy > 0) { // La case d'arrivee est en haut par rapport a celle de depart
			dy = 1;
		} else { // La case d'arrivee est alignee vericalement avec celle de depart
			dy = 0;
		}

		/*
		 * 2. On fait le chemin de la Position de depart a celle d'arrivee
		 * et on verifie qu'il n'y ai pas d'obstacle ou de soldat sur
		 * la trajectoire rectiligne.
		 */
		x = arrivee.getX();
		y = arrivee.getY();
		x0 = depart.getX();
		y0 = depart.getY();
		while (x0 != x || y0 != y) {

			x0 += dx;
			y0 += dy;
			tempP.setX(x0);
			tempP.setY(y0);

			if (!tempP.estValide())
				return false;

			if (carte.get(tempP) instanceof Element) {
				System.out.println("Tir invalide");
				return false;
			}

		}

		System.out.println("Tir valide");
		return true;
	}

	/**
	 * Utilise lors du parcours de graphes en largeur pour
	 * construire et renvoyer la liste des positions adjacentes
	 * a p1.<br />
	 * L'algorithmes verifie que chaque position est valide et
	 * vide.
	 * 
	 * @param p1       : Position dont on recherche ses positions adjacentes
	 * @param estHeros : permet de savoir si on cherches les positions adjacentes
	 *                 pour un monstre ou un heros
	 * @return la liste des positions adjacentes a p1.
	 */
	private LinkedList<Position> chercherPositionsAdjacentes(Position p1, boolean estHeros) {
		LinkedList<Position> positionsAdjacentes = new LinkedList<Position>();
		Position pos = new Position(0, 0);
		Element tempE;
		int x, y;

		x = p1.getX();
		y = p1.getY();
		for (int i = -1; i < 2; i++) {
			pos.setY(y + i);

			for (int j = -1; j < 2; j++) {
				pos.setX(x + j);
				if (pos.estValide() && !pos.equals(p1)) {

					/* Y a-t-il quelque chose a cette position ? */
					tempE = getElement(pos);
					if (estHeros) {
						if (!(tempE instanceof Element) || tempE instanceof Heros) {
							// un heros peut passer 'a travers' un autre heros
							positionsAdjacentes.add(new Position(pos.getY(), pos.getX()));
						} else if (tempE instanceof Monstres) {
							/*
							 * Le Heros peut etre deplace sur le monstre pour l'attaquer
							 * mais pas a travers (i.e on n'explore pas ses cases adjacentes)
							 */
							visite[pos.getY() - origine.getY()][pos.getX() - origine.getX()] = 0;
						}
					} else {
						if (!(tempE instanceof Element) || tempE instanceof Monstres) {
							positionsAdjacentes.add(new Position(pos.getY(), pos.getX()));
						} else if (tempE instanceof Heros) {
							visite[pos.getY() - origine.getY()][pos.getX() - origine.getX()] = 0;
							// On va en plus stocke le Heros trouve pour faire la strategie de l'ordi
							generalMonstre.herosVisibles.add((Heros) tempE);
						}
					}
				}
			}
		}
		return positionsAdjacentes;
	}

	/**
	 * Parcours en largeur des cases visibles par le soldat
	 * (i.e dans sa portee visuelle) pour determiner si elles
	 * sont accescibles.
	 * 
	 * @param s : Soldat
	 */
	public void parcoursLargeur(Soldat s) {
		int profondeur;
		int x, y;
		boolean estHeros = s instanceof Heros;
		Position pos;
		LinkedList<Position> queue;
		LinkedList<Position> positionsAdjacentes;

		/* initialisation */
		int n = s.getPortee() * 2 + 1;
		for (int i = 0; i <= n; i++) {
			Arrays.fill(visite[i], -1);
		}
		profondeur = 0;
		pos = new Position(0, 0);
		pos.setPosition(s.getPosition());

		/*
		 * visite est un carre centre autour du hero
		 * (= tous les cases qui lui sont visibles)
		 * -1 correspond a une position pas encore visitee
		 */
		origine.setX(pos.getX() - s.getPortee());
		origine.setY(pos.getY() - s.getPortee());
		if (origine.getX() < 0)
			origine.setX(0);
		if (origine.getY() < 0)
			origine.setY(0);

		queue = new LinkedList<Position>();

		/* debut du parcours */
		visite[pos.getY() - origine.getY()][pos.getX() - origine.getX()] = profondeur;
		queue.add(pos);

		while (!queue.isEmpty()) {

			pos = queue.poll();
			profondeur = visite[pos.getY() - origine.getY()][pos.getX() - origine.getX()];

			if (profondeur < s.getPortee()) {
				positionsAdjacentes = chercherPositionsAdjacentes(pos, estHeros);

				while (!positionsAdjacentes.isEmpty()) {
					pos = positionsAdjacentes.poll();
					x = pos.getX() - origine.getX();
					y = pos.getY() - origine.getY();
					if (visite[y][x] == -1) {
						visite[y][x] = profondeur + 1;
						queue.add(pos);
					}
				}
			}
		}
		/*
		 * //Affichage de la matrcie visite sur le terminal pour controller le parcours
		 * System.out.
		 * println("Voici les cases accessibles pour le Soldat selectionnee :"+n);
		 * for(int i =0; i <= n; i++) {
		 * for(int j=0; j <= n; j++) {
		 * System.out.printf("%+2d | ", visite[i][j]);
		 * }
		 * System.out.println("");
		 * }
		 * /
		 */
	}

	/**
	 * Determine si le Heros selectionne peut aller sur
	 * la Position p.
	 * 
	 * @param p : la position ou l'on bouge le heros
	 * @return
	 *         <ul>
	 *         <li>true si le Heros peut etre bouge a la position p</li>
	 *         <li>false sinon (obstacle, soldat sur le chemin)</li>
	 *         </ul>
	 */
	public boolean accessible(Position p) {
		return (visite[p.getY() - origine.getY()][p.getX() - origine.getX()] != -1);
	}

	/* -- accesseur pour le GeneralMonstre -- */
	/**
	 * Pour obtenir l'abscysse de l'origine de la Position
	 * du tableau visite.
	 * 
	 * @return la coordonnee x de origine
	 */
	public int getOrigineX() {
		return origine.getX();
	}

	/**
	 * Pour obtenir l'ordonee de l'origine de la Position
	 * du tableau visite.
	 * 
	 * @return la coordonnee y de origine
	 */
	public int getOrigineY() {
		return origine.getY();
	}

	/**
	 * Permet de savoir si une possition est accessible pour
	 * le Soldat, dont ont a prealablement fait un parcours en
	 * largeur, avec les indices du tableau visite.
	 * 
	 * @param y : indice dans le tableau visite
	 * @param x : indice dans le tableau visite[i]
	 * @return
	 *         <ul>
	 *         <li>true si la case visite[y][x] est accessible</li>
	 *         <li>false sinon (obstacle, soldat sur le chemin)</li>
	 *         </ul>
	 */
	public boolean visiteEstAccessible(int y, int x) {
		int n = 2 * ISoldat.PORTEE_VISUELLE_MAX + 2;
		if (y > 0 && x > 0 && y < n && x < n)
			return visite[y][x] != -1;

		return false; // indices incorrectes
	}
}
