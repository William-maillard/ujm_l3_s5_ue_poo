package fr.wargame.map;

import java.io.Serializable;

import fr.wargame.IConfig;

/**
 * <p>
 * Cette classe permet de manipuler une position sur une carte en 2 dimensions.
 * Et se sert de l'interface Iconfig pour les parametres de la carte.
 * </p>
 *
 * @see Carte
 * @see IConfig
 */
public class Position implements IConfig, Serializable {

	private static final long serialVersionUID = 4714573941118413671L;
	private int x, y;

	/**
	 * Cree une Position avec des coordonnees aleatoires comprise entre :
	 * <ul>
	 * <li>x appartenant a [0, LARGEUR_CARTE]</li>
	 * <li>y appartenant a [0, HAUTEUR_CARTE]</li>
	 * </ul>
	 */
	public Position() {
		x = (int) (Math.random() * (LARGEUR_CARTE - 1));
		y = (int) (Math.random() * (HAUTEUR_CARTE - 1));
	}

	/**
	 * Cree une position aux coordonnees donnees.
	 * 
	 * @param y l'ordonnee
	 * @param x l'abscysse
	 */
	public Position(int y, int x) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setPosition(Position p) {
		this.x = p.x;
		this.y = p.y;
	}

	/**
	 * Calcul si la position courante appartient a la carte.
	 * 
	 * @return
	 *         <ul>
	 *         <li>true si la position est dans la carte</li>
	 *         <li>false sinon</li>
	 *         </ul>
	 */
	public boolean estValide() {
		if (this.x >= LARGEUR_CARTE || this.y >= HAUTEUR_CARTE || this.x < 0 || this.y < 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Calcule si pos est une position voisine de la position courante
	 * 
	 * @param pos une position
	 * @return
	 *         <ul>
	 *         <li>true si pos est voisine de la position courante</li>
	 *         <li>false sinon</li>
	 *         </ul>
	 */

	public boolean estVoisine(Position pos) { // si la position p est voisine du position courant
		return ((Math.abs(x - pos.x) <= 1) && (Math.abs(y - pos.y) <= 1));
	}

	/**
	 * Calcule si pos est situe a au plus portee cases de la position courante
	 * 
	 * @param pos    une position
	 * @param portee le nombre de cases d'ecart entre pos et la position courante
	 * @return
	 *         <ul>
	 *         <li>true si pos est situe a portee cases de la position courante</li>
	 *         <li>false sinon</li>
	 *         </ul>
	 */

	public boolean estVoisine(Position pos, int portee) {
		return ((Math.abs(x - pos.x) <= portee) && (Math.abs(this.y - pos.y) <= portee));
	}

	@Override
	public String toString() {
		String s = "(" + this.x + " , " + this.y + ")";
		return s;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Position)
			return (this.x == ((Position) o).x && this.y == ((Position) o).y);

		return false;
	}

	@Override
	public int hashCode() {
		return x + y * LARGEUR_CARTE;
	}
}
