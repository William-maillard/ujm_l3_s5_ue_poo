package fr.wargame.map;

import java.awt.Graphics;

import fr.wargame.characters.Heros;
import fr.wargame.characters.Soldat;

/** Definis les fonctionnalites de bases de la carte du jeu **/
public interface ICarte {

	/**
	 * Retourne l'element situe a la position pos sur la carte
	 * 
	 * @param pos une position sur la carte
	 * @return l'element de la carte situe a pos
	 */
	Element getElement(Position pos);

	/**
	 * Trouve aleatoirement une position vide sur la carte.
	 * 
	 * @return la Position vide
	 */
	Position trouvePositionVide();

	/**
	 * Trouve une position vide choisie aleatoirement parmis
	 * les 8 positions adjacentes de pos.
	 * 
	 * @param pos : une Position
	 * @return une Position adjacente a pos
	 */
	Position trouvePositionVide(Position pos);

	/**
	 * Trouve aleatoirement un Heros sur la carte.
	 * 
	 * @return un Heros
	 */
	Heros trouveHeros();

	/**
	 * Trouve un heros choisi aleatoirement parmis les 8 positions
	 * adjacentes de pos
	 * 
	 * @param pos : une Position
	 * @return
	 *         <ul>
	 *         <li>un Heros si on en a trouve un.</li>
	 *         <li>null sinon</li>
	 *         </ul>
	 */
	Heros trouveHeros(Position pos);

	/**
	 * Permet de deplacer un Soldat sur la carte.
	 * 
	 * @param pos    : la Position d'arrivee du Soldat.
	 * @param soldat : le Soldat a deplacer.
	 * @return
	 *         <ul>
	 *         <li>true si le soldat a ete deplace.</li>
	 *         <li>false si le deplacement a ete refuse</li>
	 *         </ul>
	 */
	boolean deplaceSoldat(Position pos, Soldat soldat);

	/**
	 * Supprime le Soldat de la carte.
	 * 
	 * @param perso : le Soldat mort.
	 */
	void mort(Soldat perso);

	/**
	 * Permet de jouer un Heros. Les actions possibles sont deplacement
	 * ou attaque d'un monstre.
	 * 
	 * @param pos  : la position du Heros.
	 * @param pos2 : la position sur laquelle le heros agit.
	 * @return
	 *         <ul>
	 *         <li>true si l'action c'est realisee.</li>
	 *         <li>false si l'ation a ete refuse.</li>
	 *         </ul>
	 */
	boolean actionHeros(Position pos, Position pos2);

	/**
	 * Permet de dessiner la carte.
	 * 
	 * @param g : le contexte graphique.
	 * @param h : le Heros Selectionne par l'utilisateur.
	 */
	void toutDessiner(Graphics g, Heros h);
}