package fr.wargame.map;

import java.awt.Graphics;
import java.io.Serializable;

import fr.wargame.IConfig;
import fr.wargame.graphic.Animation;
import fr.wargame.multimedia.SpriteChargement;

/**
 * Represente Un element constituant la Carte du jeu.
 *
 * @see Obstacle
 * @see Soldat
 * @see Heros
 * @see Monstres
 */
public abstract class Element implements IConfig, Serializable {

	private static final long serialVersionUID = 3306821356239034875L;
	private Position pos;
	/** pas serialise car contien des buffered images **/
	public transient Animation animation;
	/** Permet d'acceder aux images des Soldats pour creer les animations **/
	public static final SpriteChargement sprite = new SpriteChargement();

	/**
	 * Creer un element avec une position donnee.
	 * 
	 * @param p : la position de l'element (agregation faible)
	 */
	protected Element(Position p) {
		pos = p;
	}

	// -- ACCESSEURS ET MUTATEURS --
	/** @return la position de l'element courant (agregation faible). **/
	public Position getPosition() {
		return pos;
	}

	/** @param pos : la nouvelle position de l'element (agregation faible) **/
	public void setPosition(Position pos) {
		this.pos.setPosition(pos);
	}

	// -- METHODES --
	@Override
	public String toString() {
		return "<div>" + pos.toString() + "</div>";
	}

	// -- METHODES NON IMPLEMENTEES --
	/**
	 * Dessine l'elemnt courant dans le contexte graphique passe en parametre.
	 * 
	 * @param g Graphics
	 */
	public abstract void seDessiner(Graphics g);
}
