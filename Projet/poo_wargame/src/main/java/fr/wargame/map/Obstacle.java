package fr.wargame.map;

import java.awt.Graphics;

/** Reprensente les obstacles du jeu **/
public class Obstacle extends Element {

	private static final long serialVersionUID = 9167891775338831656L;

	/** Enumere les differents type d'obstacles **/
	public enum TypeObstacle {
		BOIS, FOIN, PANIER, EAU;

		public static TypeObstacle getObstacleAlea() {
			return values()[(int) (Math.random() * values().length)];
		}
	}

	private TypeObstacle TYPE;

	/**
	 * Cree un obstacle de position pos.
	 * 
	 * @param type de l'obstacle
	 * @param pos  sa position
	 */
	public Obstacle(TypeObstacle type, Position pos) {
		super(pos);
		TYPE = type;
	}

	public TypeObstacle getType() {
		return TYPE;
	}

	@Override
	public void seDessiner(Graphics g) {
		int x = getPosition().getX() * NB_PIX_CASE;
		int y = getPosition().getY() * NB_PIX_CASE;
		// pour laisser la bordure apparente
		x++;
		y++;

		// g.setColor(TYPE.COULEUR);
		g.fillRect(x, y, NB_PIX_CASE - 1, NB_PIX_CASE - 1);
	}

	@Override
	public String toString() {
		return TYPE + " " + super.toString();
	}
}