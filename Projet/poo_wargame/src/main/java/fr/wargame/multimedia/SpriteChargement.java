package fr.wargame.multimedia;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

/** Permet de stocker et charger toutes les images du jeu **/
public class SpriteChargement {
	public BufferedImage carte1;
	public BufferedImage[] archerIdle = new BufferedImage[4];
	public BufferedImage[] archerWalk = new BufferedImage[4];
	public BufferedImage[] knightIdle = new BufferedImage[4];
	public BufferedImage[] knightWalk = new BufferedImage[4];
	public BufferedImage[] thiefIdle = new BufferedImage[4];
	public BufferedImage[] thiefWalk = new BufferedImage[4];
	public BufferedImage[] butcherIdle = new BufferedImage[4];
	public BufferedImage[] butcherWalk = new BufferedImage[4];
	public BufferedImage[] alchemistIdle = new BufferedImage[4];
	public BufferedImage[] alchemistWalk = new BufferedImage[4];
	public BufferedImage[] largeKnightIdle = new BufferedImage[4];
	public BufferedImage[] largeKnightWalk = new BufferedImage[4];
	public BufferedImage[] heavyKnightIdle = new BufferedImage[4];
	public BufferedImage[] heavyKnightWalk = new BufferedImage[4];
	
	
	public SpriteChargement(){
		getSprite();
	}
	
	private void getSprite() {
		try {
			this.carte1 = ImageIO.read(getClass().getClassLoader().getResource("tilemap.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(int i = 0; i < 4; i++) {
			try {
				this.archerIdle[i] = ImageIO.read(getClass().getClassLoader().getResource("Archer/Archer_Idle_"+(i + 1)+".png"));
				this.archerWalk[i] = ImageIO.read(getClass().getClassLoader().getResource("Archer/Archer_Walk_"+(i + 1)+".png"));
				this.knightIdle[i] = ImageIO.read(getClass().getClassLoader().getResource("Knight_Elite/EliteKnight_Idle_"+(i + 1)+".png"));
				this.knightWalk[i] = ImageIO.read(getClass().getClassLoader().getResource("Knight_Elite/EliteKnight_Walk_"+(i + 1)+".png"));
				this.butcherIdle[i] = ImageIO.read(getClass().getClassLoader().getResource("Butcher/Butcher_Idle_"+(i + 1)+".png"));
				this.butcherWalk[i] = ImageIO.read(getClass().getClassLoader().getResource("Butcher/Butcher_Walk_"+(i + 1)+".png"));
				this.thiefIdle[i] = ImageIO.read(getClass().getClassLoader().getResource("Thief/Thief_Idle_"+(i + 1)+".png"));
				this.thiefWalk[i] = ImageIO.read(getClass().getClassLoader().getResource("Thief/Thief_Walk_"+(i + 1)+".png"));
				this.alchemistIdle[i] = ImageIO.read(getClass().getClassLoader().getResource("Alchemist/Alchemist_Idle_"+(i + 1)+".png"));
				this.alchemistWalk[i] = ImageIO.read(getClass().getClassLoader().getResource("Alchemist/Alchemist_Walk_"+(i + 1)+".png"));
				this.largeKnightIdle[i] = ImageIO.read(getClass().getClassLoader().getResource("Large_Knight/LargeKnight_Idle_"+(i + 1)+".png"));
				this.largeKnightWalk[i] = ImageIO.read(getClass().getClassLoader().getResource("Large_Knight/LargeKnight_Walk_"+(i + 1)+".png"));
				this.heavyKnightIdle[i] = ImageIO.read(getClass().getClassLoader().getResource("Knight_Heavy/HeavyKnight_Idle_"+(i + 1)+".png"));
				this.heavyKnightWalk[i] = ImageIO.read(getClass().getClassLoader().getResource("Knight_Heavy/HeavyKnight_Walk_"+(i + 1)+".png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
