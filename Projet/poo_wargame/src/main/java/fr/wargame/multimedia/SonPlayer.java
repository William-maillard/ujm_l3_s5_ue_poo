package fr.wargame.multimedia;

import java.io.InputStream;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequencer;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;


/**
 * Permet de lire des musiques.
 */
public class SonPlayer{
	
	private Thread sonThread;
/* Il est necessaire de charger les files dans le module parce que sinon erreur : format non reconnu
 * En jouant plusieur fois le meme son
 **/
	
	/**
	 * Lance un thread pour jouer un son en format .midi
	 * @param path : nom du fichier sonore
	 * @param repeter <ul>
	 * 					<li>true si on veut jouer la musique en boucle</li>
	 * 					<li>false si on veut jouer la musique une fois</li>
	 * 				  </ul>
	 *
	 */
	public void midi(String path, boolean repeter){
		this.sonThread = new Thread() {
			boolean running = true;
			Sequencer player;
			public void run() {
				try {
			        InputStream ais = this.getClass().getResourceAsStream(path);
					player = MidiSystem.getSequencer();
					player.open();
					player.setSequence(ais);
					if(repeter == true) {
					  player.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
					}
					player.start();
					while(running) {}
				} catch (Exception e) {
		              System.err.println(e.getMessage());
		              
		           }				
			}
			public void interrupt() {
				player.stop();
				player.close();
				running = false;				
				
			}
		};
		sonThread.start();
	}
	
	
	/**
	 * Lance un thread pour jouer un son en format .wav
	 * @param file : nom du fichier sonore
	 * @param repeter <ul>
	 * 					<li>true si on veut jouer la musique en boucle</li>
	 * 					<li>false si on veut jouer la musique une fois</li>
	 * 				  </ul>
	 */
	public void wav(String file, boolean repeter){
		new Thread() {
			public void run() {
				try {
			        Clip player = AudioSystem.getClip();
			        player.open(AudioSystem.getAudioInputStream(this.getClass().getResourceAsStream(file)));
			        player.start();
					if(repeter == true) {
					  player.loop(Clip.LOOP_CONTINUOUSLY);
					}
					player.start();
					Thread.sleep(player.getMicrosecondLength());
					player.close();
				} catch (Exception e) {
		              System.err.println(e.getMessage());
		           }
			}
		}.start();	
	}
	
	public void stop() {
		if(this.sonThread.isAlive()) {
			this.sonThread.interrupt();
		}
	}
	
}
