package fr.wargame.multimedia;

import java.awt.Image;
import java.io.File;

import javax.swing.ImageIcon;

import fr.wargame.FenetreJeu;

/**
 * Permet de charger les ressources du menu.
 */
public class ChargementRessourcesMenu {
	public Image parchemin;
	public ImageIcon menu;
	public Image alchemist;
	public Image butcher;
	public Image archer;
	public Image thief;
	public Image largeKnight;
	public Image heavyKnight;
	public Image eliteKnight;
	public ImageIcon win;
	public ImageIcon over;
	// public Font policeBouton;

	/** Permet de charger le ressources multimedia du menu **/
	public void chargement() {

		FenetreJeu.nomsSauvegardes = new File(".sauv").list();
		parchemin = new ImageIcon(getClass().getClassLoader().getResource("parchemin.png")).getImage();
		menu = new ImageIcon(getClass().getClassLoader().getResource("home.png"));
		alchemist = new ImageIcon(getClass().getClassLoader().getResource("Alchemist/Alchemist_Idle_1.png")).getImage();
		butcher = new ImageIcon(getClass().getClassLoader().getResource("Butcher/Butcher_Idle_1.png")).getImage();
		archer = new ImageIcon(getClass().getClassLoader().getResource("Archer/Archer_Idle_1.png")).getImage();
		thief = new ImageIcon(getClass().getClassLoader().getResource("Thief/Thief_Idle_1.png")).getImage();
		largeKnight = new ImageIcon(getClass().getClassLoader().getResource("Large_Knight/LargeKnight_Idle_1.png"))
				.getImage();
		heavyKnight = new ImageIcon(getClass().getClassLoader().getResource("Knight_Heavy/HeavyKnight_Idle_1.png"))
				.getImage();
		eliteKnight = new ImageIcon(getClass().getClassLoader().getResource("Knight_Elite/EliteKnight_Idle_1.png"))
				.getImage();
		win = new ImageIcon(getClass().getClassLoader().getResource("win.png"));
		over = new ImageIcon(getClass().getClassLoader().getResource("over.png"));
		/*
		 * policeBouton = new Font(
		 * Font.createFont(
		 * Font.TRUETYPE_FONT,
		 * new FileInputStream(new
		 * File(getClass().getClassLoader().getResource("cooper_black.TTF"))
		 * )
		 * )
		 * );
		 */
		/*
		 * Font font = new Font(Font.createFont(Font.TRUETYPE_FONT,
		 * new FileInputStream(new File("5.ttf"))).
		 * getFamily(), Font.BOLD, 38);
		 */
	}

	/*
	 * public String[] creerListeSauvegarde() {
	 * Path path =
	 * FileSystems.getDefault().getPath(getClass().getClassLoader().getResource(
	 * ".sauv/").getFile());
	 * DirectoryStream<Path> fluxRepertoire = null;
	 * try {
	 * fluxRepertoire = Files.newDirectoryStream(path);
	 * } catch (IOException e) {
	 * e.printStackTrace();
	 * }
	 * 
	 * LinkedList<String> l = new LinkedList<String>();
	 * for(Path f : fluxRepertoire) {
	 * l.add(f.toString());
	 * }
	 * 
	 * String[] sauv = new String[l.size()];
	 * int i =0;
	 * for(String s : l) {
	 * sauv[i] = s;
	 * i++;
	 * }
	 * 
	 * return sauv;
	 * }
	 */
}
