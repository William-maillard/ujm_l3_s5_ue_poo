# UJM_L3_S5_UE_POO

Création d'un jeu de type wargame.

## Tools To explore for Java to webassembly
 
- teaVM <http://teavm.org/docs/tooling/maven.html> not compatible with heavy client like swwing need refactor.

- 100% compatible with java <https://labs.leaningtech.com/cheerpj2>

### maven * TeaVm

```xml
    <build>
        <plugins>
        <plugin>
            <groupId>org.teavm</groupId>
            <artifactId>teavm-maven-plugin</artifactId>
            <version>0.8.1</version>
            <dependencies>
            <!-- This dependency is required by TeaVM to emulate subset of Java class library -->
            <dependency>
                <groupId>org.teavm</groupId>
                <artifactId>teavm-classlib</artifactId>
                <version>0.8.1</version>
            </dependency>
            </dependencies>
            <executions>
            <execution>
                <?m2e execute onConfiguration?>
                <goals>
                <goal>compile</goal>
                </goals>
                <phase>process-classes</phase>
                <configuration>
                <mainClass>dessin.DessinFormes</mainClass>
                <mainPageIncluded>true</mainPageIncluded>
                <debugInformationGenerated>true</debugInformationGenerated>
                <sourceMapsGenerated>true</sourceMapsGenerated>
                                <targetType>WEBASSEMBLY</targetType>
                </configuration>

            </execution>
            </executions>
        </plugin>
        </plugins>
    </build>
```
