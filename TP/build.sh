# build the jar file with maven
mvn clean; mvn package;

# move it to the root dir to be indexing by git
mv ./target/Dessin-1.0.jar ./demo/

# compile it into webassembly with cheerpJ
python3 ../../../cheerpj_2.3/cheerpjfy.py ./demo/Dessin-1.0.jar